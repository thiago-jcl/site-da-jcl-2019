-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: jcl
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `certificados`
--

DROP TABLE IF EXISTS `certificados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificados`
--

LOCK TABLES `certificados` WRITE;
/*!40000 ALTER TABLE `certificados` DISABLE KEYS */;
INSERT INTO `certificados` VALUES (1,'Pearson Vue','Pearson Vue Teste Center Administrator Certified','Certificado','2019-10-31 17:27:15','2019-10-31 17:27:15'),(2,'Certificação em Sistemas SS7 / GSM','Certificação em Sistemas SS7 / GSM – Tekelec EAGLE / TK145 – (SS7 / IP7 Systems) – U.S.A.','Certificado','2019-10-31 17:29:48','2019-10-31 17:29:48'),(3,'Certificação em CCNA','Certificação em CCNA - Cisco Certified Network Associate.','Certificado','2019-10-31 17:30:07','2019-10-31 17:30:07'),(4,'Voice over IP, Frame Relay and ATM','Voice over IP, Frame Relay and ATM - U.S.A.','Certificado','2019-10-31 17:31:22','2019-10-31 17:31:22'),(5,'Cisco GSR (Redes de Alta Velocidade)','Cisco GSR (Redes de Alta Velocidade) - Inglaterra.','Certificado','2019-10-31 17:31:45','2019-10-31 17:31:45'),(6,'Especialização em Redes de Computadores.','Especialização em Redes de Computadores.','Certificado','2019-10-31 17:32:45','2019-10-31 17:32:45'),(7,'Especialização em Engenharia de Sistemas.','Especialização em Engenharia de Sistemas.','Certificado','2019-10-31 17:33:02','2019-10-31 17:33:02'),(8,'Registro no CREA','Registro no CREA: CREA/RN 2199-TD.','Certificado','2019-10-31 17:33:18','2019-10-31 17:33:18'),(9,'BSI','BSI – Certificação de Auditores Internos baseado na ISO-27001.','Certificado','2019-10-31 17:33:48','2019-10-31 17:33:48'),(10,'Registro no CRA-BA','Registro no CRA-BA: 03158','Certificado','2019-10-31 17:34:00','2019-10-31 17:34:00'),(11,'Tecnologia de Redes ADSL','Tecnologia de Redes ADSL – U.S.A.','Certificado','2019-10-31 17:34:47','2019-10-31 17:34:47'),(12,'Tecnologia de Redes de Alta Velocidade ATM','Tecnologia de Redes de Alta Velocidade ATM – U.S.A.','Certificado','2019-10-31 17:34:58','2019-10-31 17:34:58'),(13,'Cisco GSR (Redes de Alta Velocidade)','Cisco GSR (Redes de Alta Velocidade) - Inglaterra.','Certificado','2019-10-31 17:35:10','2019-10-31 17:35:10'),(14,'MPLS Networks and Management','MPLS Networks and Management - Inglaterra.','Certificado','2019-10-31 17:35:28','2019-10-31 17:35:28'),(15,'Enterasys Networking Security','Enterasys Networking Security - Brasil','Certificado','2019-10-31 17:35:41','2019-10-31 17:35:41'),(16,'MEDALHA DE OURO 2011','MEDALHA DE OURO 2011, concedido pela INTERMARKETING - Internacional de Marketing','Premiação','2019-10-31 17:36:07','2019-10-31 17:36:07'),(17,'MEDALHA DE OURO 2012','MEDALHA DE OURO 2012, concedido pela INTERMARKETING - Internacional de Marketing','Premiação','2019-10-31 17:37:24','2019-10-31 17:37:24'),(18,'QUALIDADE BRASIL 2012','QUALIDADE BRASIL 2012, concedido pela LAQI – Latim American Quality Institute','Premiação','2019-10-31 17:37:37','2019-10-31 17:37:37'),(19,'QUALIDADE BRASIL 2013','QUALIDADE BRASIL 2013, concedido pela LAQI – Latim American Quality Institute','Premiação','2019-10-31 17:37:54','2019-10-31 17:37:54'),(20,'PRÊMIO TOP QUALIDADE BRASIL','PRÊMIO TOP QUALIDADE BRASIL, concedido pela Academia Brasileira de Honrarias ao Mérito, 2013, órgão ligado a CICESP','Premiação','2019-10-31 17:38:10','2019-10-31 17:38:10'),(21,'PRÊMIO TOP QUALIDADE BRASIL','PRÊMIO TOP QUALIDADE BRASIL, concedido pela Academia Brasileira de Honrarias ao Mérito, 2014, órgão ligado a CICESP','Premiação','2019-10-31 17:38:24','2019-10-31 17:38:24');
/*!40000 ALTER TABLE `certificados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (6,'BSC','https://www.bracell.com/','imagem/woRLK9cfRzgBVol2qqCa1K2XaPFKIF4FCrsCVsev.jpeg','2019-10-25 17:55:22','2019-11-05 16:49:44'),(7,'Marazul Hotel','https://www.marazulhotel.com.br/pt-br/','imagem/V1vWzle2gTf5F9RqGGH01rXWcwoSoZpYkL5eZyib.jpeg','2019-10-25 17:56:09','2019-10-28 17:16:37'),(9,'Portal F','http://www.portalf.com.br/','imagem/7A5lBxVq36LrNAaDbCETKxYBleEn2pyfUYVVnPNJ.jpeg','2019-10-25 17:57:27','2019-10-28 17:15:43'),(10,'Yacht Clube - Bahia','http://yachtclubedabahia.com.br/','imagem/Berftz5f1DqMW2KkYQj6doqhsArZDIP9VfynHIdo.jpeg','2019-10-25 17:57:49','2019-10-28 17:15:25'),(13,'UFBA','https://ufba.br/','imagem/xYjhNiIKA3zMxM43XvOSZLNtZfw60j9zQkhyrDkH.jpeg','2019-10-28 16:59:19','2019-10-28 17:15:05'),(14,'Prefeitura Municipal de Salvador','http://www.salvador.ba.gov.br/','imagem/I3im9MXgRaodGNWtJAJy96p1hqksm8LtNwTwZnRn.jpeg','2019-10-28 17:00:49','2019-10-28 17:14:50'),(15,'Correios','http://www.correios.com.br/','imagem/FbAQQayK21BEIfSBfd62zc5LC73G4vz6S4yE1I9C.gif','2019-10-28 17:01:17','2019-10-28 17:14:24'),(16,'Oi Telecomunicações','https://www.oi.com.br/','imagem/0XnDiO4j1YZVVkxstCHFyzLjY5zx7FrPD42Cgb5y.gif','2019-10-28 17:03:05','2019-10-28 17:14:06'),(17,'Prefeitura de Camaçari','http://www.camacari.ba.gov.br/','imagem/NSVwjg3rS2XUE7fVAzP1s6vsIOMeQA6jbpxjr4TF.png','2019-10-28 17:03:12','2019-10-28 17:13:54'),(18,'CDL Salvador','https://cdl.com.br/','imagem/7tOk6qYDxAflYvyNiNYqmT5oBDvS5p33CyyFoB6o.jpeg','2019-10-28 17:03:57','2019-10-28 17:13:14'),(20,'Receita Federal','http://receita.economia.gov.br/','imagem/HI9Wf9msdKRUNUQvfHTI0hroxoSBWCMEOdwc4gZL.gif','2019-10-28 17:05:06','2019-10-28 17:12:51'),(21,'51º CT Exército Brasileiro','http://www.eb.mil.br/bahia','imagem/NHV19x64nXVAmhpwZVPguvhSpmKRgdaJUoJFrVij.jpeg','2019-10-28 17:05:10','2019-10-28 17:12:34');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso_users`
--

DROP TABLE IF EXISTS `curso_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `curso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso_users`
--

LOCK TABLES `curso_users` WRITE;
/*!40000 ALTER TABLE `curso_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `curso_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (2,'Linux','40','No início somente aqueles que eram considerados experts conseguiam operar o sistema operacional LINUX onde tinha um ambiente sem interface gráfica e utilizava apenas modo texto, hoje crescendo cada vez mais nos ambientes voltados para servidores e com certeza o profissional terá um grande diferencial no mercado de trabalho com um curso LINUX, venha e confira !!!','imagem/jucg4IZgQh6d5wPSqyyLi28w9Dhx8zskLCtKva8t.jpeg','2019-11-01 16:54:25','2019-11-01 16:54:25'),(3,'Preparatório CCNA','40','Um curso com os assuntos chaves de uma das certificações mais importantes no mundo de redes de computadores a nível mundial, fizemos uma seleção dos melhores temas do curso e vamos aborda-los em forma de fast track. O que está esperando? venha conferir!!','imagem/gYsFMDHW67OYXZJkAlJ74SEFK42lTsRCXVzooNUT.png','2019-11-01 16:57:25','2019-11-01 16:57:25');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fales`
--

DROP TABLE IF EXISTS `fales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assunto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensagem` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fales`
--

LOCK TABLES `fales` WRITE;
/*!40000 ALTER TABLE `fales` DISABLE KEYS */;
/*!40000 ALTER TABLE `fales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (25,'2014_10_12_000000_create_users_table',1),(26,'2014_10_12_100000_create_password_resets_table',1),(27,'2019_09_27_141146_create_cursos_table',1),(28,'2019_09_30_142415_create_servicos_table',1),(29,'2019_09_30_144400_create_produtos_table',1),(30,'2019_09_30_150910_create_noticias_table',1),(31,'2019_09_30_155648_create_testes_table',1),(32,'2019_09_30_162225_create_certificados_table',1),(33,'2019_09_30_162236_create_clientes_table',1),(34,'2019_09_30_162246_create_parceiros_table',1),(35,'2019_10_02_161612_create_fales_table',1),(36,'2019_10_08_124751_create_curso_users_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (2,'Inforum 2007','O diretor da JCL-Tecnologia, Leonardo Melo, participou como palestrante no evento do INFORUM, com palestra sobre Segurança em Redes Coorporativas.','2007-03-20','imagem/dN3VmqrmejnTHmptePyc6Wsgzhh0ifAv5VBy2cXq.jpeg','2019-10-30 15:40:48','2019-10-30 15:40:48'),(3,'Palestra da Fapesb','A palestra trata sobre a inserção da inovação tecnológica como forma de inclusão social.Sendo então lançado três editais: * Bahia Inovação – PAPPE Subvenção; * Bahia Inovação – Pesquisadores nas Empresas (Inserção de mestre e Dr. em empresas) e Apoio a Sistemas Locais de Inovação nas ICT’s. Esses editais terão um investimento de 100 milhões em empresas e Instituições de Ciência e Tecnologia da Bahia até 2010.','2008-02-19','imagem/xd9BMXbzm3ZlxdO9xzeqxyetAV7CdXr4Ky8Mj0p4.jpeg','2019-10-30 15:41:41','2019-10-30 15:41:41'),(4,'Princípios de Segurança de Redes de Computadores e Operação Básica do AKER Firewall','Nos dias 31 Outubro e 01 Novembro de 2008 a JCL Tecnologia, parceira da Aker Security Systems, ministrou o treinamento \"Princípios de Segunrança de Redes de Computadores e Operação Básica do AKER Firewall\" para colaboradores.','2008-10-31','imagem/6jdrsfwXTtsNwlpeIwH71JfxXDBs3fyQKhQt8RZA.jpeg','2019-10-30 15:42:14','2019-10-30 15:42:14'),(5,'Confraternização dos Funcionários da JCL','Confraternização dos Funcionários da JCL','2008-12-25','imagem/lXImXVRGMFxCVGeyYZvR2fasZZdZpXyplqPYvwyf.jpeg','2019-10-30 15:42:34','2019-10-30 15:42:34'),(6,'V TI Meeting','A JCL Tecnologia participou do 5° TI meeting, como um dos principais patrocinadores. Esse evento foi realizado no Bahia Othon Palace Hotel, no dia 02 de junho de 2009 em Salvador, promovido pela SUCESU, entidade de grande credibilidade e referência no cenário nacional, voltado ao seguimento de Tecnologias de Informação e Comunicação (TIC), o qual teve como tema central: “Gerenciamento Estratégico da TI”. Na ocasião o diretor da JCL, Leonardo Melo, esteve com importantes personalidades, tais com o Diretor de informática da Sucesu, Sr. Edson Luiz Leal, Diretor da DATAGEO, Sr. Saulo farias, Diretor da Inforum, Sr. Deraldo Pitombo.\"','2009-06-02','imagem/BEPlK5v7vGejDaOnVl2TpSEw7tJtq4vic73IFfRz.jpeg','2019-10-30 15:43:14','2019-10-30 15:43:14'),(7,'Com participação da JCL, a Pref. de Camaçari pode economizar até R$ 3 milhões no projeto de Software Livre de computador','Uma economia de cerca de R$ 3 milhões anuais. É o que o governo municipal pretende economizar em 2010 com a implantação de softwares livres em todos os computadores da Prefeitura.\r\n\r\nNos próximos três meses, cerca de 700 funcionários estarão fazendo cursos de capacitação para trabalhar no novo sistema operacional.\r\n\r\nAs duas primeiras turmas estão sendo capacitadas no Centro Educacional Maria Quitéria, em dois períodos, das 8h às 12h e das 13h às 17h. Os primeiros funcionários a passar pela capacitação são das secretarias de Educação (Seduc) e da Administração (Secad).','2010-01-12','imagem/aMbL0RHKN6OeEfUPVqipTlHkkoJcLEUEvWrSsSks.jpeg','2019-10-30 15:43:53','2019-10-30 15:43:53'),(8,'Lançamento do Sistema de Recadastramento - RECAD','Em 06 de maio de 2010, no auditório principal da Prefeitura Municipal de Camaçari localizada à Rua Francisco Drumond, S/N, Centro Administrativo, foi realizado o evento para lançamento do Programa de Recadastramento das Empresas e Profissionais do Município, o RECAD.\r\n\r\nO evento foi inciado com uma apresentação do Sr. Paulo Cézar secretário da SEFAZ, Secretaria da Fazenda do município e contou com a participação do Sr. Francisco Franco, secretário de Governo e representante do prefeito de Camaçari, Luiz Caetano, o sr. Pedro Faill Presidente  da Câmara de Dirigentes Logistas de Camaçari, a Sra. Marineide Araújo representante da Delegacia da Mulher, o Sr. Carlos Silveira e o Sr. Luciano Sacramento contador, e convidados.','2010-05-20','imagem/6WM7w6UgsTInQ38tIqEEQHDwfqgVNAljMXU6ojOf.jpeg','2019-10-30 15:44:19','2019-10-30 15:44:19'),(9,'Implantação do Sistema de Educação na Prefeitura de Camaçari','A JCL Tecnologia, a pedido da Prefeitura Municipal de Camaçari, está concluindo a implantação do Sistema de Educação do E-Cidade, Software Público especializado em soluções para administração de municípios.  O módulo de Educação está sendo implantado, atendendo as demandas da SEDUC – Secretaria de Educação do Município de Camaçari, voltado a gestão da Educação do município, o qual conta com mais de 40.000 alunos em 90 escolas públicas.\r\n\r\nComo parte do planejamento de implantação de novos sistemas, a JCL Tecnologia executou o mapeamento de todos os processos da SEDUC. Para isso foram realizadas reuniões com todos os gestores das Coordenadorias e Gerências da SEDUC/CAM-BA, os quais forneceram as informações necessárias para mapear os processos executados para a gestão da educação do município de Camaçari, gerando um relatório de 169 (Cento e sessenta e nove) páginas, com mapeamento de 42 processos principais e 336 atividades catalogadas entre processos e  entidades envolvidas.','2010-07-05','imagem/vpz2ZFdNYlGtrL3ZgICssT5htAF1BQbRX7M4O6LO.png','2019-10-30 15:44:45','2019-10-30 15:44:45'),(10,'Software e-cidade auxilia gestão escolar','Profissionais de escolas municipais do município de Camaçari, Bahia, participam sexta-feira (13/08/10), de um treinamento sobre o módulo de educação do Sistema de Gestão Municipal E-Cidades.\r\n\r\nA nova ferramenta, que está sendo implantada pela JCL em varias escolas do município , já está em funcionamento nas escolas São Thomaz de Cantuária, Maria Quitéria, Luiz Pereira Costa, Normal, Denise Tavares, Yolanda Pires, Ilda Leal – Caic e CEMC, todas na sede de Camaçari.','2010-08-20','imagem/0Sr8wyHncIVnzQbhHPFJC6iacdjVm2zVK1BiMQ7L.jpeg','2019-10-30 15:45:17','2019-10-30 15:45:17'),(11,'Apresentação do E-Cidade para Gestores da Educação de Camaçari','No dia 05 de maio de 2010 a JCL entregou a fase de Mapeamento de processos do Projeto E-Cidade/Educação do município de Camaçari-BA, e fez apresentação do sistema relacionando os processos levantados às suas rotinas, demonstrando facilidades e soluções através do E-Cidade. Participaram do evento os gestores da SEDUC, dentre os quais a Sub-secretária Sra. Maria das Graças, o Gerente da ASTEC-TI ( Assessoria de Tecnologia da Informação) Sr. Jean Miranda e seus demais Cordenadores e Gerentes.','2010-11-19','imagem/VXXDa1LySSpM27NTrZ5ycTUqMpTKBdjiLCjUKnEk.jpeg','2019-10-30 15:45:58','2019-10-30 15:45:58'),(12,'Evento de apresentação para implantação do Software Livre','Em dezembro de 2009 ocorreu um evento de apresentação para a implantação do Software Livre em Camaçari. Com a implantação desse sistema haverá uma redução nos custos e proporcionará mais segurança operacional e melhor desenvolvimento, sendo que a migração para o novo sistema vai acontecer em três fases: capacitação, treinamentos e migração, deixando a maquina municipal ainda mais ágil. Participaram do evento o Sr. Bruno Moura Coordenador da CCTGI (Coordenadoria Central de Tecnologia e Gestão da Informação do Município) o Diretor da JCL o Sr. Leonardo Melo, o Prefeito da cidade Sr. Luiz Caetano e demais secretários.','2010-11-22','imagem/LFTPSVf0f5uaXdM4fsFcg9jJNjMhh1DopgLQeoMm.jpeg','2019-10-30 15:46:43','2019-10-30 15:46:43'),(13,'JCL Tecnologia recebe Prêmio Medalha de Ouro à Qualidade do Brasil 2010','A JCL Tecnologia recebeu o Prêmio Medalha de Ouro à Qualidade do Brasil 2010, no segmento de Tecnologia da Informação. O evento ocorreu dia 30 de novembro, reunindo personalidades e empreendedores de diversos setores que aprimoram o mercado financeiro regional.','2010-12-20','imagem/9qrNd4pDevM1ZSQKariolkU2bH3B8bMG3Jqi0z52.jpeg','2019-10-30 15:47:13','2019-10-30 15:47:13'),(14,'Confraternização dos Colaboradores','É desse momento que a empresa precisa para comemorar as conquistas, é quando se estabelece a importância de todos, reforçando os laços que unem as pessoas por objetivos em comum.','2011-02-24','imagem/Al5pqsZSAbg7OGLqOHVpnJ9CAIAaAqAwZzVPni66.jpeg','2019-10-30 15:47:40','2019-10-30 15:47:40'),(15,'RECAD','No dia 09 de Maio de 2011, no auditório da CASA DO TRABALHO em Camaçari, às 9h30 foi lançado o RECAD (Programa de Recadastramento de Atividades) desenvolvido pela Empresa JCL – TECNOLOGIA. O evento teve a presença do Prefeito de Camaçari Sr. Luiz Caetano, do Diretor da JCL - TENOLOGIA Sr. Leonardo Melo, Secretários Municipais, representantes da Receita Federal, de Associações Empresariais do Município.\r\n\r\n \r\n\r\nO sistema visa aperfeiçoar a política tributaria de Camaçari e realizar o planejamento fiscal para os próximos anos. Para isso será realizado um recadastramento obrigatório das empresas instaladas no município.\r\n\r\n \r\n\r\nDurante o lançamento do programa, o secretario, Sr. Paulo Cezar, fez um breve retrospecto das ações da secretaria e lembrou que o sucesso do projeto depende muito dos empresários do Município, que tem um papel fundamental no processo de atualização dos dados de cada empresa.','2011-06-20','imagem/3ZVSuZvJcoH1HC3Vnj8Ba42g6aOa32lN6494oYVs.jpeg','2019-10-30 15:48:11','2019-10-30 15:48:11'),(16,'Implantação da Rede Metropolitana de Salvador do Exército Brasileiro','A JCL Tecnologia, em parceria com a Sistemas Tecnologia e a Wavenet, está de parabéns por ser um dos principais participantes da implantação da Rede Metropolitana de Salvador do Exército Brasileiro. O projeto prevê a implantação de seis POP\'s instalados em locais estratégicos da cidade de Salvador, o que possibilitará melhor desempenho nas comunicações das unidades do Exército.','2011-09-21','imagem/N7R61bmOirB3oe7u9WWP7hnp31OfZWeuyUYeDPvL.jpeg','2019-10-30 15:48:50','2019-10-30 15:48:50'),(17,'A JCL, em parceria com a UPB, patrocina Encontro de Capacitação do TCM-BA com os Gestores Municipais em Vitória da Conquista-BA','Aproximadamente 450 agentes públicos, de 63 municípios do sudoeste baiano, entre técnicos, prefeitos e vereadores, lotaram o auditório do Pólo de Educação Permanente em Saúde, na sexta-feira, (02/09), na cidade de Vitória da Conquista, durante o oitavo Encontro Regional de Capacitação do TCM-BA com Gestores Municipais.\r\n\r\nA JCL é parceira da UPB neste evento e esteve com seu Stand, apresentando o e-cidade como solução para a integração do serviço público em umsistema de gestão de municípios.\r\n\r\nO e-cidade é sistema integrante do Portal do SoftwarePúblico do Governo Federal e a JCL é um dos seus colaboradores oficiais.\r\n\r\nO evento foi excelente para a capacitação dos Gestores quanto aos processos que envolvem as contas o município e o e-cidade apresenta-se como ótima ferramenta auxiliar nestes processos.','2011-10-19','imagem/V1mltmRzcOU2T1CWQLRLMnsDwSgB9oYbs6jnq4Fz.jpeg','2019-10-30 15:49:24','2019-10-30 15:49:24'),(18,'II Encontro Nacional de Tecnologia da Informação para os Municípios','A JCL, juntamente com com a Prefeitura Municipal de Camaçari, participou de um importante evento que ocorreu em Brasília-DF nos dias 25, 26 e 27 o Encontro Nacional de Tecnologia da Informação que contou com as seguintes atividades paralelas:\r\n- II Encontro Nacional de Tecnologia da Informação para os Municípios\r\n- II Encontro Nacional de Qualidade de Software\r\n- II Encontro Nacional do Software Público Brasileiro\r\n- I Encontro de Governança em Tecnologia da Informação\r\n- I Encontro Nacional de Dados Abertos\r\n- Pré-Conferência Nacional de Governo Eletrônico','2011-11-06','imagem/0errQ9Zsk4dgA58kegbhy3DZRaC8C1LdniJDshr7.jpeg','2019-10-30 15:49:54','2019-10-30 15:49:54'),(19,'A JCL - TECNOLOGIA recebeu pela 2ª vez consecutiva, o Prêmio Medalha de Ouro à Qualidade do Brasil','No dia 30 de Novembro,no Instituto Feminino da Bahia, a JCL - TECNOLOGIA recebeu o prêmio Medalha de Ouro à Quallidade do Brasil . A premiação é voltada para os profissionais de diversos segmentos, como forma de enaltecer e reconhecer o esforço que fizeram, ao longo do ano, em prol da qualificação de seus produtos e serviços.','2011-12-13','imagem/ctyW0pe6xqv1DlkKERidC68qY5G44vx7QKZZSkVR.jpeg','2019-10-30 15:50:18','2019-10-30 15:50:18'),(20,'Inauguração da Rede Metropoltinada do Exército (RME) e Solenidade de Troca de comando do 51° CT.','No dia 19 de janeiro durante a inauguração da Rede Metropolitana do exército, a JCL-TECNOLOGIA foi homenageada pelo comando do 51° Centro de Telemática, devido o trabalho prestado na implementação da rede , bem como na ministração dos cursos de capacitação aos oficiais.\r\n\r\nHouve também a solenidade de Troca de comando do 51°CT, cuja administração estava a cargo do Cel. Eiras, o qual acompanhou todo o projeto com muito esmero e expectativa. Nesta cerimônica, o comando foi então passado ao Cel. Medlig, como operação de rotina a cada quatro anos.\r\n\r\n                                                                 \r\n\r\n Durante a cerimônia, houve o hasteamento da Bandeira do Brasil, com execução do Hino Nacional. Ao final, o comando do 51°CT ofereceu um cocktail aos convidados, onde estavam tambem presentes representantes das empresas Sistemas Tecnologia e da Wavenet.','2012-01-19','imagem/C1KySmw8UdboGAMicCFgL6YrrdPywMc9pD9j72gq.jpeg','2019-10-30 15:51:01','2019-10-30 15:51:01'),(21,'Fórum de Debate Eleições 2012','A JCL-TECNOLOGIA esteve no evento divulgando o E-cidade, Software Público de Gestão Municiapal. O E-cidade permite a informatização da gestão pública do município, facilitando a prestação de contas com o TCM, a adequação com a lei de transparência e a exportação para o SIGA(TCM-BA) e ao EDUCACENSO(MEC).\r\n\r\nSegundo o Sr. Leonardo Melo, Diretor da JCL Tecnologia, o evento foi um sucesso, com cerca de 1500 presentes, entre prefeitos, vereadores e assessores, que puderam participar das discussões sobre a legislação eleitoral e a legalidade das suas condutas, e espera com isso uma nova cultura na política e administração dos municípios.','2011-01-31','imagem/uNAKU8XgkqHRkXo4SvuyccWp1x7vXNlo4vimT9oU.jpeg','2019-10-30 15:51:42','2019-10-30 15:51:42'),(22,'A JCL TECNOLOGIA implantou um avançado Sistema de Segurança de TI na BSC (Bahia Specialty Cellulose)','A JCL TECNOLOGIA implantou um avançado Sistema de Segurança de TI na BSC (Bahia Specialty Cellulose), importante indústria de produção e única produtora de celulose solúvel especial com alto teor de pureza obtida a partir da madeira de eucalipto da América Latina. A BSC está localizada no Polo Industrial de Camaçari, Bahia. Com esse sistema todos os acessos à rede interna da BSC serão muito mais restritos, evitando o acesso de pessoas não autorizadas. Segundo o analista de sistemas Lucas Araujo, membro do departamento de TI da BSC, a segurança dos dados da empresa sempre foi uma prioridade para sua equipe, e com esse sistema implantado pela JCL TECNOLOGIA, o nível de segurança aumentou significativamente. A JCL TECNOLOGIA como representante da CISCO SYSTEM na Bahia, fez uso de sistemas tecnológicos avançados da CISCO SYSTEM, para atingir os objetivos requisitados pela BSC.','2012-11-08','imagem/546M6b1bNjNT66QWlvIfg5Sw2WhE7sJcO7TIgaZ1.jpeg','2019-10-30 15:52:14','2019-10-30 15:52:14'),(23,'A JCL TECNOLOGIA É SELECT PARTNER!','Cisco Select Certification reconhece e premia parceiros que alcançaram o Cisco Small Business Especialização ou Small Business Foundation Especialização. Cisco Select reflete o conhecimento tecnológico e de negócios de um parceiro específico para pequenas e médias empresas. A JCL TECNOLOGIA É SELECT PARTNER!','2013-05-20','imagem/VG9kkSjo1UfVRAnbmI6NIFq4YNWJDnfKIPn6u7F5.png','2019-10-30 15:52:42','2019-10-30 15:52:42'),(24,'\"COMENDA DA ORDEM, CRUZ DE RECONHECIMENTO SOCIAL E CULTURAL- NO GRAU DE GRAN COMENDADOR.\"','O diretor da JCL Tecnologia Sr. Leonardo Melo é mais uma vez homenageado pela Soberana Ordem. O evento ocorreu no dia 25/04/2013, onde Sr. Leonardo foi titulado Gran Comendador. Toda a Equipe JCL esteve presente na solenidade. \r\n\r\nA Soberana Ordem é reconhecedora dos méritos morais intelectuais específicos dos cidadãos presentes à sociedade, congregando e representando estes Intelectuais com a outorgação de novas comendas. Colaborando para a integração dos associados, na sua pluralidade, cultural, educacional religiosa e política para promover e organizar seu desenvolvimento integral como resposta aos problemas da comunidade humana em que está inserido.','2013-05-21','imagem/9l3IxVWcFlUulN2RWs4gwHgIAsJICcjyVYgUfwi4.jpeg','2019-10-30 15:53:11','2019-10-30 15:53:11'),(25,'JCL TECNOLOGIA RECEBE PRÊMIO DE QUALIDADE DA LATIN AMERICAN QUALITY INSTITUTE','A JCL Tecnologia recebeu no dia 01/06/2012 certificado de membro ativo da organização LAQI, com todos os direitos e obrigações, também foi contemplada à JCL Tecnologia o Brazil Quality Certification que é o reconhecimento dos ótimos resultados alcançados através do uso correto das ferramentas de qualidade total.\r\n\r\n\r\nA Latin American Quality Institute é uma organização privada sem fins lucrativos, seu principal objetivo é o incentivo e apoio à competitividade das empresas e organizações latino-americanas. É qualificada como a instituição mais ampla no que se refere ao desenvolvimento de normas e padrões de qualidade na América Latina é uma das mais importantes do gênero no mundo. A organização controla programas de reconhecimento para fomentar nas companhias a utilização de sistemas eficazes em seus processos. A LAQI foi escolhida pela UN como aliada estratégica na região, através da aplicação dos projetos Global Compact y PRME (Principles for Responsible Management Education), ambos pertencentes à Organização das Nações Unidas.\r\nA sede central da LAQI está localizada na Cidade do Panamá. Todos os anos a instituição organiza o evento Latin American Quality Awards, o qual ocorre no mês de novembro, além de conferências em 17 países, denominadas Quality Summit.','2013-05-22','imagem/PBr0sjekvVqr1crTIbjgV7XwNx5lwIQ2l7yKpaJH.jpeg','2019-10-30 15:54:12','2019-10-30 15:54:12'),(26,'A JCL TECNOLOGIA recebe o Prêmio top Qualidade Brasil e Selo de Ouro 2013.','A JCL TECNOLOGIA foi indicada e ranqueada entre as 100 Melhores Empresas pela Academia Brasileira de Honrarias ao Mérito, após a justa decisão do Egrégio Conselho dos Membros Acadêmicos, ao reconhecê-lo como Destaque Empresarial do Ano.\r\n\r\nO evento ocorreu dia 29 de Agosto na Mansão Hasbaya-SP.\r\n\r\nO Prêmio Top Qualidade Brasil existe desde 2004 reconhecendo empresas com o Selo “Qualidade Empresarial com Responsabilidade Social”, dentre elas, Kalunga, Roldão, Santher (SNOB e Personal), Capemisa, Johnson&Johnson, Lacta, Milani, Coopertax, Ceratti, Grupo Camargo & Mello, Bradesco Seguros, Camicado, Telhanorte, Esfiha Juventus, Tebe Concessionarias de Rodovias, Kenko Patto, Gocil, dentre outros.','2013-09-10','imagem/spkJnZkKyzsd6YtxXVSulfFo6WxcMgZ8sowXPUWk.jpeg','2019-10-30 15:55:05','2019-10-30 15:55:05'),(27,'Apresentacao do Caderneta Digital.','A JCL TECNOLOGIA no dia 11/12/2013, fez uma apresentaçao de um dos seus mais novos sistemas : o Caderneta Digital (versão Beta) aos professores da rede pública de ensino da Prefeitura Municipal de Camaçari. Esse sistema trará agilidade no controle de chamadas em salas de aula, alem de registro digital de notas e conceitos, tornando o trabalho dos professores mais simples e confiável.\r\nOs professores demonstraram satisfação com a nova ferramenta tecnológica.','2013-12-12','imagem/Q0YgJi5ZC5cig8w8FQwTjpSHd8yo5qULL40PzUe0.jpeg','2019-10-30 15:55:54','2019-10-30 15:55:54'),(28,'Seduc apresenta E-Cidade','Profissionais da Secretaria da Educação de Camaçari (Seduc) e secretários municipais conheceram nesta segunda-feira (13/09), o módulo de educação do sistema E-Cidade. A apresentação foi realizada no auditório da Prefeitura.\r\n\r\nO E-cidade é um software público de gestão municipal composto pelos módulos educação, saúde, financeiro, tributário, patrimônio e RH, que podem ser adaptados à realidade e a necessidade da administração.\r\n\r\nSegundo o secretário da Educação, Valter Lima, o sistema foi implantado recentemente e ainda está em fase de testes, mas a expectativa é que a nova ferramenta reduza custos operacionais e aumente a eficiência administrativa da Secretaria.','2014-03-27','imagem/gZqiqpPILn0TIRSX3ugwc4CzfDYSVZj9qX2Gsd0O.jpeg','2019-10-30 15:56:39','2019-10-30 15:56:39'),(29,'Treinamento E-CIDADE para Secretários Escolares e Monitores','A JCL TECNOLOGIA, nos dias 04, 05 e 06 de junho deste ano (2014), no complexo Cidade Saber em Camaçari, ministrou nova rodada de treinamentos sobre o E-CIDADE para Secretários Escolares e Monitores da Prefeitura Municipal de Camaçari. Foram ministrados treinamentos para 3 turmas com um total de 77 participantes. O treinamento foi totalmente hands-on com a participação interativa dos participantes no uso do sistema E-CIDADE, área de Educação, que o sistema mais premiado para gestão municipal. O E-CIDADE é um sistema voltado a gestão municipal, abrange todas áreas da prefeitura: Educação, Saúde, Tributação, Patrimônio e outras. O foco do treinamento foi o E-CIDADE, área de Educação. A JCL TECNOLOGIA implantou o E-CIDADE em Camaçari em 2011, município que possui mais de 110 escolas publicas e mais de 40 mil alunos e esse período tem sido um grande sucesso para o município na área educacional. Além disso a JCL implantou outras ferramentas complementares, tais como Caderneta Digital (que permite o acesso on-line a caderneta de notas e frequências), o Portal Aprender (Portal utilizado para prover interação entre alunos, professores, e a comunidade, com uso de rede social própria) e outros. A instrutora Ana Paula de Almeida, concluiu que o treinamento do e-Cidade foi de fundamental importância para a SEDUC para fortalecer o conhecimento das funcionalidades do sistema e a fundamental importância no cadastro das informações das Escolas no e-Cidade e manter essa base ativa e atualizada para que o sistema possa contribuir com a otimização dos processos educacionais e demais benefícios deforma fácil, acessível e eficaz.','2014-06-13','imagem/nqT5nqOi5qpjc40tBOh6P09w6i7MXAxNtL0eOoxZ.jpeg','2019-10-30 15:57:09','2019-10-30 15:57:09'),(30,'JCL Tecnologia, em parceria com a Multirede, é o mais novo Centro de Treinamento Cisco na Bahia','Agora de forma inédita no Norte e Nordeste do Brasil, a JCL TECNOLOGIA, em parceria com a MULTIREDE, conceituada empresa de treinamentos em TI, está oferecendo cursos certificados CISCO.\r\n\r\nA MULTIREDE é o principal parceiro educacional da Cisco no Brasil e um dos maiores na América Latina. Classificada como Cisco Learning Specialized, nível máximo obtido somente por parceiros que demonstram alto nível de especialização, é responsável pelo desenvolvimento e aplicação de programas estratégicos de treinamentos técnicos e de vendas em diversos países da América Latina além de ministrar os treinamentos convencionais de Collaboration, Borderless Networks e Data Center.\r\n\r\nA MULTIREDE é uma empresa multinacional brasileira, líder na oferta de consultoria, educação e soluções em TIC.','2014-09-03','imagem/TNAZzAwlc4BuV5gX0k4Q0o6524hZO8FYo1pNDhlM.jpeg','2019-10-30 15:57:37','2019-10-30 15:57:37'),(31,'A JCL TECNOLOGIA foi premiada pelo 2º ano consecutivo, entre as melhores empresas brasileiras do ano','Em 25/09/2014 a JCL TECNOLOGIA foi premiada pelo 2o. ano consecutivo, entre as melhores empresas brasileiras do ano pela Academia Brasileira de Honrarias ao Mérito órgão ligado a CICESP (Centro de Integração Cultural e Empresarial de São Paulo), após a justa decisão do Egrégio Conselho dos Membros Acadêmicos, ao reconhecê-lo como Destaque Empresarial do Ano. A homenagem ocorreu no auditório da Câmara Municipal de São Paulo.\r\n\r\nO Prêmio Top Qualidade Brasil existe desde 2004 reconhecendo empresas com o Selo “Qualidade Empresarial com Responsabilidade Social”, dentre elas, Kalunga, Roldão, Santher (SNOB e Personal), Capemisa, Johnson&Johnson, Lacta, Milani, Coopertax, Ceratti, Grupo Camargo & Mello, Bradesco Seguros, Camicado, Telhanorte, Esfiha Juventus, Tebe Concessionarias de Rodovias, Kenko Patto, Gocil, dentre outros','2014-10-03','imagem/4Mp2q8HgtVb40OtnJzLVT0GnjK5Wb2ffCUUowXsS.jpeg','2019-10-30 15:58:08','2019-10-30 15:58:08'),(32,'A JCL TECNOLOGIA, participou como patrocinadora e expositora do 1o congresso de Nordestino de Municípios, de 11 a 13 de novembro de 2014, em Salvador, no Centro de Convenções da Bahia.','A JCL TECNOLOGIA, participou como patrocinadora e expositora do 1o congresso de Nordestino de Municípios, de 11 a 13 de novembro de 2014, em Salvador, no Centro de Convenções da Bahia.\r\n\r\nA realização do Congresso é uma parceria da União dos Municípios da Bahia (UPB) com as demais entidades municipalistas do Nordeste e os governos estaduais. “Vamos expor os desafios enfrentados pelas prefeituras num processo interativo de trocas de experiências e estudo de alternativas, porém o mais importante é estabelecer uma agenda conjunta entre municípios da região”, afirma a presidente da UPB, prefeita Maria Quitéria.\r\nComo tema foi escolhido o mote “Oportunidades e possibilidades para o Nordeste”, que propõe um projeto coletivo de desenvolvimento para esse território, capaz de mobilizar forças políticas e sociais para ações convergentes. Maria Quitéria revela que o foco estará na implementação de estratégias de enfrentamento para os desafios do presente e do futuro.\r\n\r\nA JCL TECNOLOGIA, tem participado ativamente na propagação do uso de Tecnologia da Informação nos municípios da Bahia, especialmente no município de Camaçari, onde presta serviço a mais de 8 anos.','2014-11-11','imagem/zZnHdTSmW02ehCDQ0R3SV5hfyVnqK01vt48hQUP2.jpeg','2019-10-30 15:58:41','2019-10-30 15:58:41'),(33,'Secretaria Municipal de Educação do município de Dias D’Ávila, ocorreram oficinas para a implantação do novo Software de Gestão Escolar no Município.','No período de 28 á 31 de julho de 2015, na Secretaria Municipal de Educação do município de Dias D’Ávila, ocorreram oficinas para a implantação do novo Software de Gestão Escolar no Município, com o objetivo de capacitar usuários do sistema, com os perfis de secretárias escolares, técnicos das secretarias das unidades escolares, Diretores e Coordenadores Pedagógicos.\r\n\r\nNo final de cada oficina os presentes estarão aptos em obter notas dos alunos matriculados, histórico escolar, movimentação dos alunos, gerenciamento dos professores, acervo da biblioteca, fornecimento e organização da alimentação escolar, com possibilidade de integração direta ao almoxarifado do município e controle do transporte escolar utilizado pelos alunos. Além disso, disponibilizará relatórios para a Secretaria de Educação, auxiliando na tomada de decisões e facilitando o controle das atividades das escolas da Rede.','2015-07-28','imagem/Jb5keJmX6BLGZpRFwTutNPMVI6AWpqP5lgG5ba7y.jpeg','2019-10-30 15:59:27','2019-10-30 15:59:27'),(34,'A JCL Tecnologia no próximo dia 27/10 estará apresentando o lançamento do Sistema de Gestão Educacional implantado na Prefeitura Municipal de Dias D\'Ávila.','Afim de dinamizar o ambiente escolar, a Secretaria Municipal de Educação (SEDUC) de Dias D\'Ávila estará implantando um novo Sistema de Gestão Educacional que dará uma solução completa provendo suporte para alunos, professores, funcionários e à própria Secretaria. A solenidade de lançamento do sistema de software acontecerá na próxima terça-feira (27/10), a partir de 09:00h e terá as presenças de autoridades políticas e servidores da SEDUC como diretores, coordenadores pedagógicos e professores e da equipe de Desenvolvimento de Sistemas de Software da JCL TECNOLOGIA.\r\n\r\nAtravés do uso do SGE, Sistema de Gestão Educacional, módulo Educação, é possível fazer o histórico escolar, transferências, matrículas entre outros serviços, diminuindo o tempo de entrega dos documentos, pois as informações do estudante já estão cadastradas no sistema. Desta forma, a Secretaria da Educação (SEDUC) pode fazer um mapeamento mais detalhado da situação escolar do Município.\r\n\r\nO SGE é um sistema multifinalitário para gestão educacional, baseado no sistema do software público. Ele é composto das seguintes subáreas: Alimentação Escolar, Biblioteca, Escola, Secretaria e Transporte Escolar; O SGE também integra a gestão municipal abrangendo as seguintes áreas: Assistência Social, Cidadão, Configuração, Financeiro, Gestor, Patrimonial, Recursos Humanos, Saúde e Tributário;\r\n\r\nAlém do SGE, um importante sistema que será implantando na SEDUC é o Portal Educação que é um ambiente colaborativo de conteúdos Educacionais. Ele abrange todas as funcionalidades de uma rede social como, por exemplo: conexões, amizades, grupos temáticos, mensagens privadas, fóruns de discussão, comentários, postagens, divulgações, entre outras. O Portal Educação facilita a interatividade da Escola entre os alunos, professores, pais e a comunidade e permite o acompanhamento de quaisquer projetos com um ambiente de interação simples, dinâmico e eficiente.','2015-10-23','imagem/yG2KRWJD9ywbvZ7aYiTxoVSPbrT00Dp8kCVGedV9.jpeg','2019-10-30 15:59:56','2019-10-30 15:59:56'),(35,'SGE e Portal Educação são lançados em solenidade no auditório da Prefeitura Municipal de Dias d\'Ávila','Aconteceu na manhã desta terça-feira (27) a solenidade de lançamento do Sistema de Gestão Educacional (SGE) e do Portal Educação, no auditório da Prefeitura Municipal de Dias d\'Ávila, frutos do comprometimento da Secretaria Municipal de Educação (SEDUC) em agilizar e otimizar a gestão educacional da cidade com uso das inovações tecnológicas.\r\n\r\nO secretário de educação Marcelino Almeida abriu o evento ressaltando a importância da implantação do SGE e do Portal Educação na rede municipal de ensino, “que facilitará o acompanhamento completo das atividades de toda a secretaria, desde a parte administrativa até a produção de conteúdos pelos professores e alunos, além de oferecer aos pais diversos serviços através da internet como matrículas, histórico escolar e transferências, evitando-se assim o deslocamento até as escolas e as conseqüentes filas”.','2015-10-28','imagem/LKyAiS6GtCSYfCqceuZYY2ukMIam8O9Z2VtNN7I7.jpeg','2019-10-30 16:00:24','2019-10-30 16:00:24'),(36,'O evento da UVB ocorreu nos dias 26 e 27/11 no Hotel BahiaMar em Salvador-Ba.','A JCL Tecnologia participou do evento da UVB ( União dos Vereadores do Brasil) no Congresso Brasileiro de Vereadores em Salvador nos dias 26/11 e 27/11, no Hotel BahiaMar em Salvador-Ba.','2015-12-03','imagem/Sq3x53DShw0ai5pf7nr6RCRyA8L2I80eZHAr25Wm.jpeg','2019-10-30 16:00:53','2019-10-30 16:00:53'),(37,'MEDALHA DE OURO A QUALIDADE BRASIL 2016','Em 28 de novembro de 2016, a JCL TECNOLOGIA recebeu a premiação MEDALHA DE OURO A QUALIDADE BRASIL 2016 outorgado pela INTERMARKETING, sob chancela da Soberana Ordem da Sociedade Intercontinental de Ciências Humanas, Jurídicas e Sociais, como Destaque Empresarial do Ano 2016, escolhida entre as Melhores Empresas do ano no ramo de Tecnologia da Informação. O evento, realizado no auditório do Centro de Cultura na Câmera Municipal de Salvador, contou com presenças de autoridades civis e militares e da sociedade local.\r\n\r\nNa ocasião o Sr. Leonardo Melo, diretor executivo da JCL TECNOLOGIA, dedicou o PRÊMIO MEDALHA DE OURO À QUALIDADE BRASIL 2016 aos seus clientes e parceiros e agradeceu a seus colaboradores e sem os quais esta outorga não seria possível.','2016-11-28','imagem/RLHjfLksfqdqxPbWRC2y9Ax0CJVilmyftLxQQvEp.jpeg','2019-10-30 16:01:27','2019-10-30 16:01:27'),(38,'Lançamento Da Nova Versão Do Sistema de Gestão Educacional – SGE','No dia 12/09, no auditório da prefeitura, fora realizado um encontro com as secretárias e assistentes das unidades de ensino do município, para apresentação da nova versão do Sistema de Gestão Educacional – SGE.\r\n\r\nEstiveram presentes ao evento: o Sr. Marcelino Teodoro, Secretário de Educação; a Sra. Margarete de Jesus, Gerente de Gestão Escolar; O Sr. Valmir Catarino, Coordenador do SGE; a equipe da JCL TECNOLOGIA, empresa responsável pela implantação e suporte ao sistema, com a presença do diretor Leonardo Melo, o gerente de projetos de software Carlos Botelho e os analistas Maricélia Araújo, Ronaldo Lopes e Raul Barbosa.\r\n\r\nO evento foi um sucesso!','2017-10-19','imagem/PhuoFFZvAJPSTJesSDHwhKB0nIQli211hG7M6B96.jpeg','2019-10-30 16:01:53','2019-10-30 16:01:53'),(39,'Apresentação do Sistema de Monitoramento Social (REMOS)','No dia 24/10/2017 a JCL TECNOLOGIA apresentou o Sistema de Monitoramento Social (REMOS) à nova equipe da Secretaria de Desenvolvimento e Proteção Social (SEDES) da Prefeitura de Dias D\'Ávila, gerida pela Sra. Aleide Ferreira. Na ocasião participaram gerentes e coordenadores da SEDES, onde foram apresentadas novas funções do sistema REMOS pela equipe de Software da JCL TECNOLOGIA liderada por Carlos Botelho. Participaram também a Coordenadora dos Programas Sociais Roberta Franco da SEDES e os analistas Maricelia Araújo e Ronaldo Batista da JCL.','2017-10-30','imagem/g6BBipcxoPTrIlNOCSMq1vNr4YsEz1GZruviAJ7w.jpeg','2019-10-30 16:02:16','2019-11-14 18:50:19'),(40,'Seminário de Redes e Tecnologias','Seminário de Redes e Tecnologias promovido pela SEPLAN de Lauro de Freitas: A JCL Tecnologia, através de seu Diretor Técnico Leonardo Melo, apresentou um de seus cases de grande sucesso: Projeto e implantação da Rede Metropolitana (WMAN) do Exército em Salvador, dia 02-08-2018.','2018-08-09','imagem/SDmxWrLNbetzTRgrxoPZrXwgQG2NDfpv0reFrfYr.jpeg','2019-10-30 16:02:44','2019-10-30 16:02:44');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parceiros`
--

DROP TABLE IF EXISTS `parceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parceiros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parceiros`
--

LOCK TABLES `parceiros` WRITE;
/*!40000 ALTER TABLE `parceiros` DISABLE KEYS */;
INSERT INTO `parceiros` VALUES (2,'Cisco','Cisco Systems','imagem/wdlOdCkBEausBLaUY1DaHmuAx7hZIA3dA8i6Az64.gif','2019-11-01 15:21:15','2019-11-01 15:21:15'),(3,'VUE','Pearson Vue','imagem/kHY2NT8Dt2nN1IO3dINOZl9ltM2NDcbWTpshdXvg.gif','2019-11-01 15:21:35','2019-11-01 15:21:35'),(6,'Microsoft','Microsoft','imagem/bQJwLEgGFr8A32Qzn5pECINJKWpz4pxD8wAplTDB.jpeg','2019-11-01 15:23:15','2019-11-01 15:23:15'),(7,'WEBSID','WEBSID Internet','imagem/XrYKp7qFRT4K4n6JKQ0JKPhuwPbSUNhI5GqQyplt.png','2019-11-01 15:24:06','2019-11-01 15:24:06'),(9,'Kaspersky','Kaspersky','imagem/r7yqlUjZxrXCim0s7kbHAOV5mYSUyTBLImnahBT9.gif','2019-11-01 15:25:25','2019-11-01 15:25:25'),(11,'HP','HP','imagem/LnHECfuV65NG4zGznb84tUFd8FVLu6kX2q2v6Ay2.jpeg','2019-11-01 15:25:51','2019-11-01 15:25:51'),(12,'Bitdefender','Bitdefender','imagem/CPnppRPEb36BPEFm744UXgiSSb0XdM3JKKiRZnZs.jpeg','2019-11-01 15:26:14','2019-11-01 15:26:14'),(13,'Network1','Network1','imagem/x3KPe5ugJZOaZYPTbtqsy1fN7vPjB4NMVEwi7r2m.jpeg','2019-11-01 15:27:32','2019-11-01 15:27:32'),(14,'Handytech','Handytech','imagem/hmuQaOXm34YiYJT9BrB3ViEybRyfwuEVYBiUHbHm.jpeg','2019-11-01 15:27:53','2019-11-01 15:27:53'),(15,'HPE','HPE','imagem/UAnhTPwKFMAntxsFFEn11K8peziDNQ5PvdCH02n3.jpeg','2019-11-01 15:28:08','2019-11-01 15:28:08'),(16,'Kryterion','Kryterion','imagem/aBVdhXNqExTkmetdPW1un0bISndkCKABzuUW4IXM.jpeg','2019-11-01 18:03:24','2019-11-01 18:03:24');
/*!40000 ALTER TABLE `parceiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (2,'Recad','Desenvolvimento','Sistema de Recadastramento de Atividades','Software','imagem/2rCh6NYFMUXMpZMQgx6oogMGa9SNwzyR19qzNS0k.jpeg','2019-10-30 16:26:56','2019-10-30 16:26:56'),(3,'Siprof','Desenvolvimento','Sistema de Produtividade Fiscal','Software','imagem/Lfy4jrLCWdvoMC0GUCy9EZIeTU3k0KR7SFeEzTnA.jpeg','2019-10-30 16:27:31','2019-10-30 16:27:31'),(6,'Caderneta Digital','Desenvolvimento','Caderneta Digital','Software','imagem/njdGkK26pMJ7VnKlyTbzSeyieWC4bRCWfZ9HTvWx.png','2019-10-30 16:37:22','2019-11-01 18:17:11'),(7,'Portal Educação','Desenvolvimento','Portal Educação','Software','imagem/CRun9NvvfHCGvxaRf43OXfeEcDIE2REdfVfMedLn.png','2019-10-30 16:37:58','2019-11-01 18:17:06'),(10,'Remos','Desenvolvimento','Rede de Monitoramento Social','Software','imagem/dxcqY5bsvVuqfAYwSwAes14Z4rq0EsY70l31TWQI.png','2019-11-19 17:01:54','2019-11-19 17:01:54'),(13,'SGE','Desenvolvimento','Sistema de Gestão Educacional','Software','imagem/OERmswr7eG787VTsOdoSnqqOCGETnT89hfPEj9vG.jpeg','2019-11-19 17:20:16','2019-11-19 17:20:16'),(14,'Cisco','Desenvolvimento','Cisco','Segurança','imagem/2Hw7NVDNvsnBIxrTauw35yFaT3aFK6g93k5uelhc.gif','2019-11-21 19:31:45','2019-11-21 19:31:45'),(15,'Kaspersky','Desenvolvimento','Kaspersky','Segurança','imagem/DXpGALp7B97F4j0296g5RLPohuzehXGYwNCUzbVU.gif','2019-11-21 19:32:07','2019-11-21 19:32:07'),(16,'BitDefender','Desenvolvimento','BitDefender','Segurança','imagem/HuVxCd496lJ5pbNiyil2WxZfiKwRMPt6mLMeJ02R.jpeg','2019-11-21 19:32:34','2019-11-21 19:32:34'),(17,'Sophos','Desenvolvimento','Sophos','Segurança','imagem/7mnNYTlrq821H9HTsinAbSlYe9yhKhuTZpB8Pod8.jpeg','2019-11-21 19:32:56','2019-11-21 19:32:56'),(18,'Microsoft','Desenvolvimento','Microsoft','Segurança','imagem/GDtJLYc2hGIrydg6GjYNqktPoOtLlY8pTJyZwqAQ.jpeg','2019-11-21 19:33:10','2019-11-21 19:33:10'),(20,'HP','Desenvolvimento','HP','Segurança','imagem/XnM5MVCofgiO6Y5hyUwOX7OCQjO0mGpeKCTtOQfj.jpeg','2019-11-21 19:33:43','2019-11-21 19:33:43');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'Auditoria e Segurança em Sistemas de TI e Rede','A JCL possui profissionais com experiência em realização de auditorias em sistemas de TI e Redes, com o objetivo de levantar laudos e relatórios de avaliação desses sistemas, em todos os níveis, desde o nível físico ao nível de aplicação dos sistemas de rede (considerando o modelo OSI). Falhas, vulnerabilidades e outros pontos que possam interferir no funcionamento dos sistemas, são avaliados e relatados quando necessário. O alto nível de profissionalização de nossos consultores garante serviços com alto sigilo e confiabilidade.','imagem/6SLKQ8bCm2UrirlveelYdns472EAaiCKA8lxkg9X.jpeg','2019-10-29 16:29:35','2019-10-29 16:29:35'),(2,'Governança Coorporativa em TI','Dada à importância crescente da Tecnologia da Informação nas empresas, torna-se cada vez mais necessário se ter uma Governança de TI eficaz que garanta a criação de valor de TI para os negócios da empresa. Na realidade, a Governança de TI propõe não somente a criação de valor de TI, mas também a sua preservação.','imagem/i2cfr8fIPEAOdA502zInl0cdkxwtaOzA7c6TgUnn.png','2019-10-29 16:33:56','2019-10-29 16:33:56'),(3,'Desenvolvimento de Sistemas especiais para Governo','Através de análise criteriosa de necessidades especiais do ambiente de TI governamental, a JCL desenvolve sistemas de software para necessidades especiais, os quais são produzidos de forma customizada, de acordo com a necessidade dos clientes, tais como Sistemas de Controle de Produtividade de Funcionários, Sistemas de Cadastramento de Contribuites, etc.','imagem/rtH6lP57iRFdDJFZrKQdTMTYtUwBIMKcexj5zYQe.jpeg','2019-10-29 16:34:20','2019-10-29 16:34:20'),(4,'Soluções em TI, Redes e Telecomunicações','Nossa empresa oferece aos seus clientes, Projetos e Soluções Gerais em Redes e Telecomunicações. A empresa participa ativamente em soluções voltadas as diversas tecnologias. Possui um corpo técnico altamente qualificado, possuindo certificações obtidas em empresa nacionais e internacionais.','imagem/y71Lw3fvQu2rhzF04mLTKkRuMIDLbQ50eKAB1c7f.jpeg','2019-10-29 20:46:11','2019-10-29 20:46:11'),(5,'Treinamento em TI, Redes e Telecom','No moderno mercado de Redes e Telecom, é necessário que os profissionais sejam treinados por empresas especializadas nessas áreas e que possam prover preparação teórica e prática das mais atuais tecnologias. A JCL possui profissionais altamente qualificados, com certificações obtidas nos EUA e na Europa.','imagem/MHWUZVbMEXQAVGYI54ShqF45P6zmx7WpSylq2md9.jpeg','2019-10-30 15:39:44','2019-10-30 15:39:44');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testes`
--

DROP TABLE IF EXISTS `testes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testes`
--

LOCK TABLES `testes` WRITE;
/*!40000 ALTER TABLE `testes` DISABLE KEYS */;
INSERT INTO `testes` VALUES (1,'Pearson Vue','https://home.pearsonvue.com/test-taker.aspx','imagem/IVCNKpsMc1mEmmEHMax3ykqzJBTwFyTz6lufRLwj.gif','2019-11-01 16:48:45','2019-11-01 16:50:49'),(3,'Kryterion','https://www.kryteriononline.com/test-taker/testing-center-support','imagem/xaHLnWD7DKYFFcQlxnArQJZ01Cd7xcBh0m5PGZ8U.jpeg','2019-11-19 17:32:13','2019-11-19 17:32:13');
/*!40000 ALTER TABLE `testes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@gmail.com','$2y$10$ji9CqaUhGCncLIQFh0a/SeFhrrpxGs4QnNSb6mkNnqwt2/AMqGvpG','qcoTiP8hUIzE8IpF8z2PILL5ocPrKoLgXazVpUMtUTgRusTWDvW6HabCJeir',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-28  9:35:36
