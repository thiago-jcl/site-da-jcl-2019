<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('admin.home');



/*  Curso */

Route::get('/admin/curso','CursoController@index')->name('admin.curso');
Route::post('/admin/curso/cadastrar','CursoController@cadastrar')->name('admin.curso.cadastrar');
Route::get('admin/curso/perfil/{id}','CursoController@perfil')->name('perfil');
Route::post('admin/curso/editar','CursoController@editar')->name('curso/editar');
Route::delete('admin/curso/deletar','CursoController@delete');

/* Servico */

Route::get('/admin/servico','ServicoController@index')->name('admin.servico');
Route::post('/admin/servico/cadastrar','ServicoController@cadastrar')->name('admin.servico.cadastrar');
Route::get('admin/servico/perfil/{id}','ServicoController@perfil')->name('perfil');
Route::post('admin/servico/editar','ServicoController@editar')->name('servico/editar');
Route::delete('admin/servico/deletar','ServicoController@delete');

/* Produto */

Route::get('/admin/produto','ProdutoController@index')->name('admin.produto');
Route::post('/admin/produto/cadastrar','ProdutoController@cadastrar')->name('admin.produto.cadastrar');
Route::get('admin/produto/perfil/{id}','ProdutoController@perfil')->name('perfil');
Route::post('admin/produto/editar','ProdutoController@editar')->name('produto/editar');
Route::delete('admin/produto/deletar','ProdutoController@delete');

/* Noticia */

Route::get('/admin/noticia','NoticiaController@index')->name('admin.noticia');
Route::post('/admin/noticia/cadastrar','NoticiaController@cadastrar')->name('admin.noticia.cadastrar');
Route::get('admin/noticia/perfil/{id}','NoticiaController@perfil')->name('perfil');
Route::post('admin/noticia/editar','NoticiaController@editar')->name('noticia/editar');
Route::delete('admin/noticia/deletar','NoticiaController@delete');

/* Teste */

Route::get('/admin/teste','TesteController@index')->name('admin.teste');
Route::post('/admin/teste/cadastrar','TesteController@cadastrar')->name('admin.teste.cadastrar');
Route::get('admin/teste/perfil/{id}','TesteController@perfil')->name('perfil');
Route::post('admin/teste/editar','TesteController@editar')->name('teste/editar');
Route::delete('admin/teste/deletar','TesteController@delete');

/* Certificado */

Route::get('/admin/cert','CertController@index')->name('admin.cert');
Route::post('/admin/cert/cadastrar','CertController@cadastrar')->name('admin.cert.cadastrar');
Route::get('admin/cert/perfil/{id}','CertController@perfil')->name('perfil');
Route::post('admin/cert/editar','CertController@editar')->name('cert/editar');
Route::delete('admin/cert/deletar','CertController@delete');

/* Cliente */

Route::get('/admin/cliente','ClienteController@index')->name('admin.cliente');
Route::post('/admin/cliente/cadastrar','ClienteController@cadastrar')->name('admin.cliente.cadastrar');
Route::get('admin/cliente/perfil/{id}','ClienteController@perfil')->name('perfil');
Route::post('admin/cliente/editar','ClienteController@editar')->name('cliente/editar');
Route::delete('admin/cliente/deletar','ClienteController@delete');

/* Parceiros */

Route::get('/admin/parceiros','ParceiroController@index')->name('admin.parceiros');
Route::post('/admin/parceiros/cadastrar','ParceiroController@cadastrar')->name('admin.parceiros.cadastrar');
Route::get('admin/parceiros/perfil/{id}','ParceiroController@perfil')->name('perfil');
Route::post('admin/parceiros/editar','ParceiroController@editar')->name('parceiros/editar');
Route::delete('admin/parceiros/deletar','ParceiroController@delete');

/* Relatorio */

Route::get('/admin/relatorios','RelatorioController@index')->name('admin.relatorios');
Route::delete('admin/relatorios/deletar','RelatorioController@delete')->name('admin.relatorios.deletar');


/* Usuario */


Route::get('/servico','User\ServicoController@index')->name('servico');

Route::get('/', 'User\HomeController@index')->name('/');

Route::get('/sobre','User\SobreController@index')->name('sobre');

Route::get('/perfil/{id}','User\HomeController@perfil')->name('perfil');

Route::get('/produto', 'User\ProdutoController@index')->name('produto');

Route::get('/curso', 'User\CursoController@index')->name('curso');
Route::post('/curso/cadastrar','User\CursoController@cadastrar')->name('curso.cadastro');

Route::get('/noticia', 'User\NoticiaController@index')->name('noticia');
Route::get('/cliente', 'User\ClienteController@index')->name('cliente');
Route::get('/parceiro', 'User\ParceiroController@index')->name('parceiro');
Route::get('/teste', 'User\TesteController@index')->name('teste');
Route::get('/cert', 'User\CertController@index')->name('cert');
Route::get('/prem', 'User\PremController@index')->name('prem');
/* Fale Conosco */

Route::get('/fale', 'User\FaleController@index')->name('fale');
Route::post('/fale/envio','User\FaleController@cadastrar')->name('fale.envio');


Route::get('/trabalhe', 'User\TrabalheController@index')->name('trabalhe');
Route::post('/trabalhe/envio','User\TrabalheController@cadastrar')->name('trabalhe.envio');

Route::get('/loja', 'User\LojaController@index')->name('loja');


