<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>JCL-Tecnologia</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.1/css/all.css">
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
<!-- Custom styles for this template -->
<link href="{{ asset('css/simple-sidebar.css')}}" rel="stylesheet">





<nav class="navbar navbar-light bg-light">
    <button class="btn btn-primary" id="menu-toggle" style="background-color:white; color:#003366; border:none;"><i class="fa fa-align-justify"></i> Menu</button>
    <a class="navbar-brand">
        <img src="{{ asset('img/favicon.ico')}}" width="30" height="30" class="d-inline-block align-top" alt="">
        JCL Tecnologia
    </a>
</nav>

<body>

    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="border-right" id="sidebar-wrapper">
            <div class="list-group list-group-flush">
                <a href="{{route('admin.home')}}" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fa fa-home"></i> Inicio</a>
                <a href="{{route('admin.servico')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-cogs"></i> Serviços</a>
                <a href="{{route('admin.produto')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-box-open"></i> Produtos</a>
                <a href="{{route('admin.curso')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-book-open"></i> Cursos</a>
                <a href="{{route('admin.noticia')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-newspaper"></i> Noticias</a>
                <a href="{{route('admin.teste')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-clipboard-list"></i> Centro de Testes</a>
                <a href="{{route('admin.cert')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-certificate"></i> Certificações</a>
                <a href="{{route('admin.cliente')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="fas fa-users"></i> Clientes</a>
                <a href="{{route('admin.parceiros')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="far fa-handshake"></i> Parceiros</a>
                <a href="{{route('admin.relatorios')}}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;"><i class="far fa-address-card"></i> Solicitações</a>
                <a href="{{ route('logout') }}" class="list-group-item list-group-item-action" style="border:none;  color:#003366;" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i>
                 
                    Sair
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
        </div>
        </div>
        <div class="container">

            @yield('content')
        </div>

        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

            
        </script>
</body>