<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>JCL-Tecnologia</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.1/css/all.css">
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<!-- Custom styles for this template -->
<link href="{{ asset('css/simple-sidebar.css')}}" rel="stylesheet">





<nav class="navbar navbar-light bg-light">
    <button class="btn btn-primary btn-small" id="menu-toggle" style="background-color:white; color:#003366; border:none;"><b><i class="fas fa-bars"></i>  Menu</b></button>
    <a class="navbar-brand">
        <img src="{{ asset('img/logo2.jpg')}}" width="30" height="50" class="d-inline-block align-top" alt="">
        JCL Tecnologia
    </a>
</nav>

<body>

    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="border-right" id="sidebar-wrapper">
            <div class="list-group list-group-flush">
                <a href="/" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fa fa-home"></i> Inicio</a>
                <a href="/servico" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-cogs"></i> Serviços</a>
                <a href="/produto" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-box-open"></i> Produtos</a>
                <a href="/curso" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-book-open"></i> Cursos</a>
                <a href="/noticia" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-newspaper"></i> Noticias</a>
                <a href="/teste" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-clipboard-list"></i> Centro de Testes</a>
                <a href="/cert" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-certificate"></i> Certificações</a>
              <a href="/prem" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-award"></i> Premiações</a>

                <a href="/cliente" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-users"></i> Clientes</a>
                <a href="/parceiro" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="far fa-handshake"></i> Parceiros</a>
                <a href="/fale" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="far fa-envelope"></i> Fale Conosco</a>
<a href="/trabalhe" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-hands-helping"></i> Trabalhe Conosco</a> 

               <a href="/loja" class="list-group-item list-group-item-action" style="border:none; color:#003366;"><i class="fas fa-store-alt"></i> Loja Virtual</a>


            </div>
        </div>

        <div class="container">

            @yield('content')
        </div>

        <script>
           
            $("#menu-toggle").click(function() {
                $("#wrapper").toggleClass("toggled");
            });
        </script>
</body>
