@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossos Certificados
  </div>
</div>
<br>
<p style="color:#003366;">Certificados</p>
@foreach($cert as $certs)
@if($certs->tipo == "Certificado")
<div class="card">
        <div class="row no-gutters">
            <div class="col-auto">
                <div class="card-body">
                    <h5 class="card-title"style="color: #003366;"><i class="fas fa-certificate"></i> {{$certs->nome}}</h5>
                    <p class="card-text" style="font-size:13px;">{{$certs->descricao}}</p>
                </div>
            </div>
        </div>
  </div>
<br>
@endif
@endforeach


@stop
