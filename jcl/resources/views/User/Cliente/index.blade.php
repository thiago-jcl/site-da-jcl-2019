@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossos Clientes
  </div>
</div>
<br>
<p style="color:#003366;"></p>
@foreach($cliente as $clientes)
<div class="card">
    <div class="row no-gutters">
        <div class="col-auto">
            <img src="{{ asset('storage/'.$clientes->imagem)}}" style="width:100px; height:100px;" class="img-fluid" alt="">
        </div>
        <div class="col">
<div class="card-body"> 
                <h5 class="card-title" style="color:#003366;">{{ $clientes->nome }}</h5>
                <p class="card-text" style="font-size:13px;">Conheça:
<a href="{{$clientes->descricao}}"> Clique Aqui</a>
</p>
        </div>
</div>
    </div>
</div>
<br>
@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $cliente->links("pagination::bootstrap-4") }}
  </ul>
</nav>

@stop
