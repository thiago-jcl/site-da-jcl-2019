@extends('layouts.layout')

@section('content')

<div class="card">
    <div class="card-header" style="font-weight:bold; color:#003366;">
        Trabalhe Conosco
    </div>
</div>
<br>
<div>
    <form method="POST" action="{{route('trabalhe.envio')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col">
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <input type="text" name="nome" class="form-control" placeholder="Nome Completo">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <input type="text" name="telefone" class="form-control" placeholder="Telefone">
            </div>
        </div>
        <br>

 <div class="row">
            <div class="col">
                <input type="text" name="area" class="form-control" placeholder="Cargo Desejado">
            </div>
        </div>
        <br>
<div class="row">
            <div class="col">
                <input type="text" name="link" class="form-control" placeholder="Linkedin">
            </div>
        </div>
        <br>

        <div class="form-group">
            <textarea rows="4" cols="50" name="curriculo" class="form-control" placeholder="Descreva seu Curriculo">
</textarea>
        </div>
</div>
<br>
<div class="modal-footer">
    <button type="submit" class="btn btn-success">Enviar</button>
</div>
</div>
</form>
<div>
    @stop
