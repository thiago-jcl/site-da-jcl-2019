@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossos Serviços
  </div>
</div>
<br>
<p style="color:#003366;">Set-List dos principais serviços prestados por nossos consultores</p>
@foreach($servico as $servicos)
<div class="card">
    <div class="row no-gutters">
        <div class="col-auto">
            <img src="{{ asset('storage/'.$servicos->imagem)}}" style="width:300px;" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-7">
            <div class="card-body">
                <h5 class="card-title" style="color:#003366;">{{ $servicos->nome }}</h5>
                <p class="card-text text-justify" style="font-size:13px; "><b>Descrição:</b> {{ $servicos->descricao }}</p>
            </div>
        </div>
    </div>
</div>
<br>
@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    {{ $servico->links("pagination::bootstrap-4") }}
  </ul>
</nav>


@stop
