@extends('layouts.layout')

@section('content')

<style>

@media (max-width: 767.98px) { 
  
  .img{
   
   width:130px;
   height:130px;
   
}

 }

@media (min-width: 992px) { 
 
 .img{
   
  height: 125px;
  width:200px;
  
  }

 }

</style>

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
   Centro de Testes
  </div>
</div>
<br>
<p style="color:#003366;"></p>
@foreach($teste as $testes)
<div class="card">
        <div class="row no-gutters">
            <div class="col-auto">
                <img src="{{ asset('storage/'.$testes->imagem)}}" class="img img-fluid" alt="Responsive image">
            </div>
            <div class="col">
                <div class="card-body">
@if($testes->nome == "Pearson Vue")
                    <h5 class="card-title"style="color: #003366;">{{$testes->nome}}</h5>
<p class="card-text text-justify" style="font-size:13px;"> Person Vue é uma empresa líder em educação no mundo e está autorizada a administrar e aplicar com exclusividade os testes (exames) referentes às certificações internacionais do The Institute of Internal Auditors (The IIA).

Os centros de testes da Pearson Vue estão preparados para as aplicações dos estudos de casos do programa Qualification in Internal Audit Leadership (QIAL).</p>
<br>                   
 <p class="card-text" style="font-size:13px; text-align:justify"><a href="{{$testes->descricao}}">Clique aqui e agende seu exame</a> </p>
                </div>
@else
 <h5 class="card-title"style="color: #003366;">{{$testes->nome}}</h5>
<p class="card-text" style="font-size:13px; text-align:justify">Kryterion</p>
<br>
 <p class="card-text" style="font-size:13px; text-align:justify"><a href="{{$testes->descricao}}">Clique aqui e agende seu exame</a> </p>
                </div>

@endif
            </div>
        </div>
  </div>

<br>
@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $teste->links("pagination::bootstrap-4") }}
  </ul>
</nav>

@stop
