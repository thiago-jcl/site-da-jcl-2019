@extends('layouts.layout')

@section('content')

<div class="card">
    <div class="card-header" style="font-weight:bold; color:#003366;">
        Nossos Cursos
    </div>
</div>
<br>
<p style="color:#003366;"></p>
@foreach($curso as $cursos)
<div class="card">
    <div class="row no-gutters">
        <div class="col-md-5">
            <img src="{{ asset('storage/'.$cursos->imagem)}}" style="width:300px;" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-7">
            <div class="card-body">
                <h5 class="card-title" style="color:#003366;">{{ $cursos->nome }}</h5>
                <p class="card-text text-justify" style="font-size:13px;">Descrição: {{ $cursos->descricao }}</p>
                <p class="card-text" style="font-size:13px;">Carga Horária: {{ $cursos->carga }}</p>
                <div class="text-right">
                    <button type="button" class="btn btn-success btn-sm" data-id="{{$cursos->id}}" data-toggle="modal" data-target="#modalExemplo"><i class="fas fa-plus"></i> Inscreva-se</button>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cadastro no Curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('curso.cadastro')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col">
                            <input type="text" name="nome" class="form-control" placeholder="Digite seu Nome Completo">
                        </div>
                        <input type="hidden" name="curso" value="{{$cursos->id}}">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <input type="email" name="email" class="form-control" placeholder="Digite seu Email">
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="telefone" class="form-control telefone" placeholder="Digite seu Telefone">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="estado" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-8">
                            <select name="cidade" class="form-control">
                            </select>
                        </div>
                    </div>
                    <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Salvar</button>
            </div>

        </div>
        </form>
    </div>
</div>

@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $curso->links("pagination::bootstrap-4") }}
  </ul>
</nav>

<script>
    jQuery("input.telefone")
        .mask("(99) 9999-9999?9")
        .focusout(function(event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });
</script>
<script>
    $.getJSON('https://servicodados.ibge.gov.br/api/v1/localidades/estados/', {
        id: 10,
    }, function(json) {

        var options = '<option value="">Estado</option>';

        for (var i = 0; i < json.length; i++) {

            options += '<option data-id="' + json[i].id + '" value="' + json[i].nome + '" >' + json[i].nome + '</option>';

        }

        $("select[name='estado']").html(options);

    });


    $("select[name='estado']").change(function() {

        if ($(this).val()) {
            $.getJSON('https://servicodados.ibge.gov.br/api/v1/localidades/estados/' + $(this).find("option:selected").attr('data-id') + '/municipios', {
                id: $(this).find("option:selected").attr('data-id')
            }, function(json) {

                var options = '<option value="">Cidade</option>';

                for (var i = 0; i < json.length; i++) {

                    options += '<option value="' + json[i].nome + '" >' + json[i].nome + '</option>';

                }

                $("select[name='cidade']").html(options);

            });

        } else {

            $("select[name='cidade']").html('<option value="">–  –</option>');

        }

    });
</script>

<script>
    $(document).ready(function() {
        $(".select[name='cidade']").select2();
    });
</script>

@stop
