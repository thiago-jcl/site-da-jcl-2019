@extends('layouts.layout')

@section('content')
<style>
@media(max-width:767.98px){

.img-2{

margin-top:10px;

}

}

</style>
<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Sobre Nós
  </div>
</div>
<br>
<p class="text-justify">
A JCL Tecnologia, empresa de Consultoria e Auditoria em Tecnologia da 
Informação e Redes, possui consultores com mais de 20 anos de experiência 
em TIC (Tecnologia da Informação e Comunicação).
Nossa empresa tem forte atuação nos segmentos de Segurança da Informação, 
Auditoria, Desenvolvimento de Sistemas, 
Projetos de Redes e Governança de TI para o mercado privado e governamental. 
Através de um conjunto de soluções integradas na área de TI, 
oferecemos proteção aos ativos de informação das empresas em níveis operacionais 
e de gestão, abrangendo desde o seu completo gerenciamento funcional, 
até um minucioso processo de engenharia social e organizacional.

</p>
<div class="container">
<div class="row">
<div class="col-md-6">
        <img src="{{ asset('img/logomarca.png')}}" class="img-fluid" alt="Responsive image">
</div>
<div class="col-md-6">
    <img src="{{ asset('img/jcl-institucional.jpg')}}" style="width:244px; height:206px;" class="img-fluid img-2" alt="Responsive image">
</div>
</div>
</div>
      <br>

  <div class="container">
    <div class="row text-right" style="margin-left:-15px;margin-right:0px;">
      <h5 style="color: #003366; text-uppercase font-family:Tw Cen MT, Arial, Helvetica, sans-serif; font-size:20px;">Principais Projetos</h5>
    </div>
  </div>
  <br>
  <div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-network-wired"></i> Implantação da nova rede lógica da Receita Federal, em todo estado da Bahia e Sergipe</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Fazendo uso de tecnologia ultra moderna, através de novos dispositivos de rede, tais como Enterasys Matrix X8, C2K122, C2G170 e BG3124. Esses dispositivos, de alta capacidade chegam a um throughput interno de até 1,28 Tera-bps (1,28 Trilhões de bits por segundo). No momento estamos concluindo a fase de implantação da rede, e estaremos iniciando em breve a fase Operação Assistida, para monitoramento da rede implantada.</p>
      </div>
    </div>
  </div>
<br>


<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-lock"></i> Auditoria na rede interna da Secretaria da Fazenda da Prefeitura Municipal de Camaçari</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Auditoria de todos os sistemas de TI voltados a tributação e cadastro da Prefeitura Municipal de Camaçari, bem como desenvolvimento de sistemas web voltados ao controle de cadastros dos contribuintes do município. Governança de TI aplicada a municípios.</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-lock"></i> Auditoria e Avaliação do Backbone IP da Prefeitura Municipal de Camaçari</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Foi efetuada Auditoria e avaliação da rede do cliente do nível de aplicação ao nível físico. O objetivo foi gerar um relatório completo de todas as características da rede do cliente, dando ênfase as vulnerabilidades em nível de segurança da rede, a qual possui aproximadamente 1000 pontos de rede.</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-project-diagram"></i> Projeto de implantação, treinamento e consultoria da rede campus LAN dos CORREIOS em Salvador</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Uma rede com 1900 pontos ativos de redes, com uso de tecnologias modernas, tais como: conexões via fibras-óticas de 1 Gbps, switches-core com redundância plena, 802.1q, 802.1d, 802.1d, etc. No momento o projeto está na fase de treinamento e implantação de sistemas de gerenciamento. Os principais elementos usados nesse projeto foram Switch-routers 3COM modelo 8800 e switches 3COM modelo 4400, bem como Sistema de Gerenciamento de Redes Network Director (3COM).</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-server"></i> Desenvolveu o Projeto de Backbone IP para a NGN (Next Generation Network, Release 4 para GSM) para a OI/Telemar (2006)</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Esse projeto propiciou interligar 14 centrais GSM da OI, a nível nacional, ao backbone IP/MPLS da Telemar. Os principais elementos usados nesse projeto foram: Cisco Routers 7609 e 6509 (OSRs), Nokia Media Gateways (MGWs) e Mobile Switch Centers (MSS).</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-project-diagram"></i> Desenvolvimento do Projeto de Segurança de Data Center desenvolvido para o Internet Data Center da Telemar (2004)</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Os principais elementos usados nesse projeto foram: Cisco PIX Firewall 525, Catalyst 5200, Catalyst 2400 e Cisco router 7507.</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-project-diagram"></i> Participação do Projeto do STJ - Superior Tribunal de Justiça, em Brasília (2000)</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Rede ATM com MPOA. Auxiliou no desenvolvimento do Capacity Planning da rede do STJ. Os principais elementos usados nesse projeto foram: Catalyst 8500, 6500, 2900 e 1900. Ministração do pacote completo de treinamentos referentes à tecnologia de MPOA e LAN Emulation.</p>
      </div>
    </div>
  </div>
<br>

<div class="card" style="border:none; background-color:white;">
    <div class="row no-gutters">
        <div class="card-block px-2" style="margin-top:6px;">
        <h5 class="text-uppercase font-weight-bold" style="color:#003366; font-size:15px"><i class="fas fa-server"></i> Implantação e gerência de toda rede LAN e WAN do SERPRO nas regiões Bahia e Sergipe</h5>
          <p class="card-text text-justify" style="color:#003366; font-size:13px;">Executamos Implantação e Treinamento aos colaboradores da empresa, voltados focando serviços de gerenciamento remoto para toda rede de equipamentos ativos.</p>
      </div>
    </div>
  </div>
<br>



@stop
