@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossas Premiações  </div>
</div>
<br>
<p style="color:#003366;">Premiações</p>

<img src="{{asset('img/selo.png')}}" class="rounded float-left" style=" width:200px; height:200px;" alt="">
<img src="{{asset('img/selo2.png')}}" class="rounded mx-auto d-block" style="height:200px;width:200px;" alt="...">

<br>


<img src="{{asset('img/selo3.png')}}" class="rounded float-left" style=" width:200px; height:200px;" alt="">
<img src="{{asset('img/selo4.png')}}" class="rounded mx-auto d-block" style="height:200px;width:200px;" alt="...">

<br>
@foreach($prem as $prems)
@if($prems->tipo == "Premiação")
<div class="card">
        <div class="row no-gutters">
             <div class="col-auto">
                <div class="card-body">
                    <h5 class="card-title"style="color: #003366;"><i class="fas fa-award"></i> {{$prems->nome}}</h5>
                    <p class="card-text" style="font-size:13px;">{{$prems->descricao}}</p>
                </div>
            </div>
        </div>
  </div>
<br>
@endif
@endforeach
@stop
