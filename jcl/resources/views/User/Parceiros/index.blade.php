@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossos Parceiros
  </div>
</div>
<br>
<p style="color:#003366;"></p>
@foreach($parceiro as $parceiros)
<div class="card">
        <div class="row no-gutters">
            <div class="col-auto">
                <img src="{{ asset('storage/'.$parceiros->imagem)}}" style="height:150px; widht:150px;" class="img-fluid" alt="">
            </div>
            <div class="col">
               <div class="card-body">
                    <h5 class="card-title"style="color: #003366;">{{$parceiros->nome}}</h5>
                    <p class="card-text" style="font-size:13px; text-align:justify">Descrição: {{$parceiros->descricao}}</p>
                </div>
            </div>
        </div>
  </div>
<br>
@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $parceiro->links("pagination::bootstrap-4") }}
  </ul>
</nav>

@stop