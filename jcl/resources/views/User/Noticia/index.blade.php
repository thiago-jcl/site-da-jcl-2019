@extends('layouts.layout')

@section('content')
<style>
@media (min-width: 992px) { 
  .img-fluid{
    height:150px;
    width:200px;
   margin-top:20px;
   overflow:hidden;
 }
 
.galeria img:hover {
  -webkit-transform: scale(1.1);
  transform: scale(1.5);
   
  
}

.galeria img{
   width:200px;
   -webkit-transition: -webkit-transform .5s ease;
   transition: transform .5s ease;
}

}

	</style>


<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossas Noticias
  </div>
</div>
<br>
<p style="color:#003366;"></p>
@foreach($noticia as $noticias)
<div class="card">
    <div class="row no-gutters">
        <div class="col-auto galeria" >

            <img id="{{$noticias->id}}" src="{{ asset('storage/'.$noticias->imagem)}}" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col">
            <div class="card-body">
  <h5 class="card-title" style="color:#003366;">{{ $noticias->nome }}</h5>
                <p class="card-text text-justify" style="font-size:13px;"><b>Descrição:</b> {{ $noticias->descricao }}</p>
<p class="card-text"> {{ date('d-m-Y', strtotime($noticias->data)) }}</p>
            </div>
        </div>
    </div>
</div>
<br>
@endforeach
<nav>
<ul class="pagination justify-content-center">
  {{ $noticia->links("pagination::bootstrap-4") }}
  </ul>
</nav>


@stop
