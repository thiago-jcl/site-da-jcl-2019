@extends('layouts.layout')

@section('content')

<div class="card">
    <div class="card-header" style="font-weight:bold; color:#003366;">
        Fale Conosco
    </div>
</div>
<br>
<div>
    <form method="POST" action="{{route('fale.envio')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col">
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <input type="text" name="nome" class="form-control" placeholder="Nome">
            </div>
        </div>
        <br>
        <div class="form-group">
            <textarea rows="4" cols="50" name="mensagem" class="form-control" placeholder="Mensagem"></textarea>
        </div>
</div>
<br>
<div class="modal-footer">
    <button type="submit" class="btn btn-success">Enviar</button>
</div>
</div>
</form>
<div>
    @stop