@extends('layouts.layout')

@section('content')

<style>
  .img-responsive {
   display: block;
   height: auto;
   max-width: 100%;
}

@media (min-width: 992px) {
   .teste {
   height: 130px;
   width: 200px;
}
 }
  </style>
<div class="card">
  <div class="card-header" style="font-weight:bold; color:#003366;">
    Nossos Produtos
  </div>
</div>
<br>
<p style="color:#003366; font-weight:bold;">Produtos-Software</p>

  @foreach($produto as $produtos)
@if($produtos->tipo == "Software")
<div class="card">
    <div class="row no-gutters">
        <div class="col-auto">
		    <img src="{{ asset('storage/'.$produtos->imagem)}}" class="img-fluid teste" alt="Responsive image">
        </div>
        <div class="col-md-7">
            <div class="card-body">
                <h5 class="card-title" style="color:#003366;">{{ $produtos->nome }}</h5>
                <p class="card-text" style="font-size:13px;">Descrição: {{ $produtos->descricao }}</p>
@if($produtos->url  != "Desenvolvimento")
<p><a href="{{$produtos->url}}" style="font-size:13px;">PDF: Clique Aqui</a></p> 
@endif           
              </div>
        </div>
    </div>
</div>
<br>
@endif
@endforeach
<p style="color:#003366; font-weight:bold;">Produtos-Segurança</p>
  @foreach($produto as $produtos)
    @if($produtos->tipo == "Segurança")
    <div class="card">
    <div class="row no-gutters">
        <div class="col-auto">
            <img src="{{ asset('storage/'.$produtos->imagem)}}"  class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-7">
            <div class="card-body">
                <h5 class="card-title" style="color:#003366;">{{ $produtos->nome }}</h5>
                <p class="card-text" style="font-size:13px;">Descrição: {{ $produtos->descricao }}</p>
            </div>
        </div>
    </div>
</div>
<br>
  @endif
@endforeach
  

@stop
