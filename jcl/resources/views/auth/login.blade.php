<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="{{ asset('css/login.css')}}" rel="stylesheet">

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="{{asset('img/logomarca.png')}}" style="heigth:100px; width:100px;" id="icon" alt="User Icon" />
    </div>


 <form class="form-horizontal" method="POST" action="{{ route('login') }}">
{{ csrf_field() }}
                    

      <input type="text" id="email" name="email" class="fadeIn second" placeholder="Email">
      <input type="password" id="password"  name="password" class="fadeIn third" placeholder="Senha">
      <input type="submit" class="fadeIn fourth" value="Entrar">
</form>
                </div>
            </div>
        </div>
    </div>
</div>

