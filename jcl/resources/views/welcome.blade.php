@extends('layouts.layout')

@section('content')
<style>

@media(max-width: 767.98px) { 
.sobre{

width:450px;
height:200px;

}

}

@media (min-width:768px){

.sobre{

width:250px;
height:200px;
}

}
@media (max-width:768px){

.img1{


height:50px;
width:150px;
}

</style>

<section>
<div style="background-color:#003366;">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
     <a href="/produto"> <img src="{{ asset('img/sid=1.jpg')}}" class="d-block w-100" alt="...">
</a>  
  </div>

     <div class="carousel-item">
    <a href="/servico">
      <img src="{{ asset('img/sid=3.jpg')}}" class="d-block w-100" alt="...">
  </a>   
</div>
<div class="carousel-item">
      <a href="/produto">
      <img src="{{ asset('img/sid=4.jpg')}}" class="d-block w-100" alt="...">
  </a>   
</div>
<div class="carousel-item">
 <a href="/servico">
      <img src="{{ asset('img/sid=5.jpg')}}" class="d-block w-100" alt="...">
</a>   
 </div>

<div class="carousel-item">
 <a href="/servico">
      <img src="{{ asset('img/sid=6.jpg')}}" class="d-block w-100" alt="...">
</a>    
</div>

   </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<br>
</section>
<section>
<div class="row">
  <div class="col-6">
    <a href="/loja">
      <img src="{{ asset('img/lojavirtual.jpg')}}" class="img-fluid img1" alt="Responsive Image">
    </a>
  </div>
  <div class="col-6">
  <a href="/produto">
      <img src="{{ asset('img/IMGSoftwareGoverno.jpg')}}" class="img-fluid img1" alt="">
    </a>
      </div>
</div>
<br>
</section>
<section>
  <div class="container">
    <div class="row text-right" style="margin-left:-15px;margin-right:0px;">
      <h5 style="color: #003366; text-uppercase font-family:Tw Cen MT, Arial, Helvetica, sans-serif; font-size:23px;">Sobre Nós</h5>
    </div>
  </div>

  <div class="card" style="border:none; background-color:#003366;">
    <div class="row no-gutters">
      <div class="col-auto">
        <img src="{{ asset('img/jcl-institucional.jpg')}}" class="sobre img-fluid" alt="Responsive image">
      </div>
      <div class="col">
        <div class="card-block px-2">
          <br>
        <h5 class="text-uppercase font-weight-bold" style="color:white;">Seja Bem Vindo</h5>
          <p class="card-text" style="color:white;">A JCL Tecnologia, empresa de Consultoria e Auditoria em Tecnologia da Informação e Redes, possui consultores com mais de 20 anos de experiência em TIC (Tecnologia da Informação e Comunicação).</p>
          <div class="text-right" style="margin-top:-10px;">
            <a href="/sobre" type="button" class="btn btn-sm" style="background-color:white; color:#003366;"><i class="fas fa-users"></i> Saiba Mais</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <br>
  <div class="container">
    <div class="row text-right" style="margin-left:-15px;margin-right:0px;">
      <h5 style="color: #003366;text-uppercase font-family:Tw Cen MT, Arial, Helvetica, sans-serif; font-size:23px;">Últimas Notícias</h5>
    </div>
  </div>


  <div class="row">
    <div class="col">
      @foreach($home as $homes)
      <div class="card" style="border:none;">
        <div class="row no-gutters">
          <div class="col-auto">
            <img src="{{ asset('storage/'.$homes->imagem)}}" style="height:150px; width:200px;" class="img-fluid" alt="">
          </div>
          <div class="col">
            <div class="card-block px-2">
              <h5 class="card-title" style="color: #003366; font-family:Tw Cen MT, Arial, Helvetica, sans-serif; font-size:18px;">{{$homes->nome}}</h5>
              <p class="card-text"> {{ date('d-m-Y', strtotime($homes->data)) }}</p>
              <div class="text-right">
                <button type="button" class="btn btn-info btn-sm" data-id="{{$homes->id}}" onclick="visualizar($(this).data('id'))" data-toggle="modal" data-target="#modalPerfil"><i class="fas fa-id-badge"></i> Detalhes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>
      @endforeach
<div class="text-right">
  <a href="/noticia" type="button" class="btn btn-sm" style="background-color:white; color:#003366; border:none;"> Ver Mais <i class="fas fa-angle-double-right"></i></a>
    </div>
  </div>
</div>
</section>

<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Detalhes da Noticia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <p>Nome: <b id="nome"></b></p>
          <p>Descrição: <b id="descricao"></b></p>
        </div>

      </div>
    </div>
  </div>
</div>


<section>

  <footer class="page-footer font-small teal pt-4" style="background-color:#003366; margin-top:15px;">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3" style="color:white;">

          <!-- Content -->
          <h5 class="text-uppercase font-weight-bold">Fale Conosco</h5>
          <p>Mande-nos suas sugestões, dúvidas e criticas através do formulário.
    Se preferir pelo nosso e-mail: <a href="#" style="color:white;"><br>
contato@jcl-tecnologia.com.br</a></p>
          <p>Fone/Fax:<b> +55 (71) 3017-3270</b></p>
          <p>Endereço:<a href="https://www.google.com.br/maps/place/JCL+Tecnologia/@-13.0071726,-38.4904086,17z/data=!4m13!1m7!3m6!1s0x7160355a36db7cf:0x23f876d86e389d88!2sTv.+Remanso+-+Rio+Vermelho,+Salvador+-+BA,+41950-700!3b1!8m2!3d-13.0071726!4d-38.4882199!3m4!1s0x0:0xd02fca533537df82!8m2!3d-13.0072051!4d-38.488137" style="color:white">
              <b> Trav. Remanso,60(Ao lado da rua do canal) - Rio Vermelho - Salvador/BA</b> <i class="fa fa-map-marker"></i></a></p>
        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-3" style="color:#003366;">
          <div class="card">
            <div class="col-md-12">
              <!-- Content -->
              <h5 class="text-uppercase text-center font-weight-bold">Redes Sociais</h5>
              <p class="text-center" style="margin-left:-25px;">Siga-nos</p>
              <br>
              <div class="text-center">
              <a href="https://www.facebook.com/jcltecnologia/"><i class="fab fa-facebook" style="font-size:30px;"></i></a>
              <a href="https://www.instagram.com/jcltecnologia/?hl=pt-br"><i class="fab fa-instagram" style="font-size:30px; margin-left:25px;"></i></a>
              <br>
              <Br>
              </div>
            </div>
          </div>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3" style="color:white; font-size:10px;">Copyright 2019 © JCL Tecnologia - Todos os Direitos Reservados
    </div>
    <!-- Copyright -->

  </footer>


</section>


<script>
  function visualizar(perfil) {

    $.ajax({
      type: 'get',
      url: '/perfil/' + perfil,
      success: function(response) {

        $('#nome').html(response.nome);
        $('#descricao').html(response.descricao);
        $('#imagem').html(response.imagem);

      },
      error: function(erro) {
        console.log(erro);
      }


    });

  }
</script>
@stop
