@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold;">
    Centro de Testes
    <div class="text-right" style="margin-top: -25px;">
      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalExemplo"><i class="fas fa-plus"></i> Novo Teste</button>
    </div>
  </div>
</div>
<br>
@foreach($teste as $testes)
<div class="card mb-3">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="{{ asset('storage/'.$testes->imagem)}}" class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">{{ $testes->nome }}</h5>
        <p class="card-text">{{ $testes->descricao }}</p>
        <div class="text-right">
          <button type="button" class="btn btn-info btn-sm" data-id="{{$testes->id}}" onclick="visualizar($(this).data('id'))" data-toggle="modal" data-target="#modalPerfil"><i class="fas fa-id-badge"></i> Detalhes</button>
          <button type="button" class="btn btn-warning btn-sm" data-id="{{$testes->id}}" onclick="editarTeste($(this).data('id'))" data-toggle="modal" data-target="#modaleditar"><i class="fas fa-edit"></i> Editar</button>
          <button type="button" class="btn btn-danger btn-sm" data-id="{{$testes->id}}" onclick="deletarTeste($(this).data('id'))" data-toggle="modal" data-target="#modalDeletar"><i class="fas fa-trash"></i> Excluir</button>
        </div>
      </div>
    </div>
  </div>
</div>

@endforeach

<!--cadastro -->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Novo Teste</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('admin.teste.cadastrar') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="row">
            <div class="col">
              <input type="text" name="nome" class="form-control" placeholder="Nome do Teste">
            </div>
          </div>
          <br>
          <div class="form-group">
            <textarea rows="4" cols="50" name="descricao" class="form-control" placeholder="Descrição do Teste"></textarea>
          </div>
          <br>
          <div class="form-group">

            <input type="file" name="imagem" class="form-control-file" id="exampleFormControlFile1">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success">Salvar</button>
      </div>
    </div>
    </form>
  </div>
</div>
<!--ver perfil -->

<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Detalhes do Serviço</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <p>Nome: <b id="nome"></b></p>
          <p>Descrição: <b id="descricao"></b></p>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Deletar -->
<div class="modal fade" id="modalDeletar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <form action="/admin/teste/deletar" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            {{method_field('delete')}}
            <div class="modal-body">

              <center><i style="font-size: 70px; color: yellow;" class="fa fa-exclamation-circle"></i></center>
              <center>
                <h3>Deseja realmente apagar o serviço?</h3><br>
              </center>

              <input type="hidden" name="id_testes" id="id_testes">

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Salvar mudanças</button>
            </div>
      </div>
      </form>
    </div>
  </div>
</div>


<!--editar -->

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Editar Serviço</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('teste/editar')}}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <input type="hidden" id="idEdit" name="editar" />
          <div class="modal-body">
            <!-- Dados Gerais -->
            <div class="form-group">
              <label>Nome do Serviço</label>

              <input name="nome" id="nomeEdit" class="form-control" require="" aria-required="true" type="text">

            </div>
            <div class="form-group">
              <label>Descrição</label>

              <textarea name="descricao" id="descricaoEdit" class="form-control" require="" aria-required="true"></textarea>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Salvar</button>
            </div>
        </form>
      </div>
    </div>
  </div>



  <script>
    function visualizar(perfil) {

      $.ajax({
        type: 'get',
        url: 'teste/perfil/' + perfil,
        success: function(response) {

          $('#nome').html(response.nome);
          $('#descricao').html(response.descricao);
          $('#imagem').html(response.imagem);
        },
        error: function(erro) {
          console.log(erro);
        }


      });

    }

    function editarTeste(perfil) {

      $.ajax({
        type: 'get',
        url: 'teste/perfil/' + perfil,
        success: function(response) {
          $('#idEdit').val(response.id);
          $('#nomeEdit').val(response.nome);
          $('#descricaoEdit').val(response.descricao);

        },
        error: function(erro) {
          console.log(erro);
        }


      });
    }

    function deletarTeste(perfil){

$.ajax({
    type: 'get',
    url: 'teste/perfil/'+ perfil,
    success: function(response) {
        $('#id_testes').val(response.id);
    },
    error: function(erro) {
        console.log(erro);
    }


});
}
  </script>
  @stop