@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold;">
    Solicitações
  </div>
  </div>
  
<br>
@foreach($relatorio as $relatorios)

<div class="card">
  <div class="card-header" style="font-weight:bold;">
  <h5 class="card-title">Nome: {{ $relatorios->nome }}</h5>



  </div>
    <div class="col-md-8">
      <div class="card-body">
        <p class="card-text" style="font-size:13px;">Email: {{ $relatorios->email }}</p>
        <p class="card-text" style="font-size:13px;">Telefone: {{ $relatorios->telefone }}</p>
      <p class="card-text" style="font-size:13px;">Cidade: {{ $relatorios->cidade }}</p>
      <p class="card-text" style="font-size:13px;">Estado: {{ $relatorios->estado }}</p>
@foreach($curso as $cursos)
@if($relatorios->curso == $cursos->id) 
      <p class="card-text" style="font-size:13px;">Curso: {{ $cursos->nome }}</p>
    @endif
@endforeach
      </div>
      </div>
  </div>
<br>
@endforeach



<div class="modal fade" id="modalDeletar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <form action="/admin/relatorios/deletar" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            {{method_field('delete')}}
            <div class="modal-body">

              <center><i style="font-size: 70px; color: yellow;" class="fa fa-exclamation-circle"></i></center>
              <center>
                <h3>Deseja realmente apagar o relatorio?</h3><br>
              </center>

              <input type="hidden" name="id_CursoUser" id="id_CursoUser">

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Salvar mudanças</button>
            </div>
      </div>
      </form>
    </div>
  </div>
</div>



<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $relatorio->links("pagination::bootstrap-4") }}
  </ul>
</nav>



<script>

    function deletarRelatorio(perfil){

$.ajax({
    type: 'get',
    url: 'relatorios/perfil/'+ perfil,
    success: function(response) {
        $('#id_CursoUser').val(response.id);
    },
    error: function(erro) {
        console.log(erro);
    }


});
}
  </script>

@stop
