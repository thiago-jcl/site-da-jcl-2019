@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header" style="font-weight:bold;">
    Noticias
    <div class="text-right" style="margin-top: -25px;">
      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalExemplo"><i class="fas fa-plus"></i> Nova Noticia</button>
    </div>
  </div>
</div>
<br>
@foreach($noticia as $noticias)
<div class="card mb-3">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="{{ asset('storage/'.$noticias->imagem)}}" class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">{{ $noticias->nome }}</h5>
      </div>
        <div class="text-right">
          <button type="button" class="btn btn-info btn-sm" data-id="{{$noticias->id}}" onclick="visualizar($(this).data('id'))" data-toggle="modal" data-target="#modalPerfil"><i class="fas fa-id-badge"></i> Detalhes</button>
          <button type="button" class="btn btn-warning btn-sm" data-id="{{$noticias->id}}" onclick="editarNoticia($(this).data('id'))" data-toggle="modal" data-target="#modaleditar"><i class="fas fa-edit"></i> Editar</button>
          <button type="button" class="btn btn-danger btn-sm" data-id="{{$noticias->id}}" onclick="deletarNoticia($(this).data('id'))" data-toggle="modal" data-target="#modalDeletar"><i class="fas fa-trash"></i> Excluir</button>
        </div>
    </div>
  </div>
</div>

@endforeach

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  {{ $noticia->links("pagination::bootstrap-4") }}
  </ul>
</nav>

<!--cadastro -->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nova Noticia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('admin.noticia.cadastrar') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="row">
            <div class="col">
              <input type="text" name="nome" class="form-control" placeholder="Nome da Noticia">
            </div>
          </div>
          <br>
          <div class="form-group">
            <textarea rows="4" cols="50" name="descricao" class="form-control" placeholder="Descrição da Noticia"></textarea>
          </div>
          <br>
          <div class="form-group">
             <input type="date" name="data" class="form-control">
          </div>
          <div class="form-group">

            <input type="file" name="imagem" class="form-control-file" id="exampleFormControlFile1">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success">Salvar</button>
      </div>
    </div>
    </form>
  </div>
</div>
<!--ver perfil -->

<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Detalhes da Noticia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <p>Nome: <b id="nome"></b></p>
          <p>Descrição: <b id="descricao"></b></p>
          <p>Data: <b id="data"></b></p>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Deletar -->
<div class="modal fade" id="modalDeletar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <form action="/admin/noticia/deletar" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            {{method_field('delete')}}
            <div class="modal-body">

              <center><i style="font-size: 70px; color: yellow;" class="fa fa-exclamation-circle"></i></center>
              <center>
                <h3>Deseja realmente apagar o serviço?</h3><br>
              </center>

              <input type="hidden" name="id_noticia" id="id_noticia">

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Salvar mudanças</button>
            </div>
      </div>
      </form>
    </div>
  </div>
</div>


<!--editar -->

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Editar Noticia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('noticia/editar')}}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <input type="hidden" id="idEdit" name="editar" />
          <div class="modal-body">
            <!-- Dados Gerais -->
            <div class="form-group">
              <label>Nome do Serviço</label>

              <input name="nome" id="nomeEdit" class="form-control" require="" aria-required="true" type="text">

            </div>
            <div class="form-group">
              <label>Descrição</label>

              <textarea name="descricao" id="descricaoEdit" class="form-control" require="" aria-required="true"></textarea>

            </div>
            <div class="form-group">
              <label>Data do Serviço</label>

              <input name="data" id="dataEdit" class="form-control" require="" aria-required="true" type="date">

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Salvar</button>
            </div>
        </form>
      </div>
    </div>
  </div>



  <script>
    function visualizar(perfil) {

      $.ajax({
        type: 'get',
        url: 'noticia/perfil/' + perfil,
        success: function(response) {

          $('#nome').html(response.nome);
          $('#descricao').html(response.descricao);
          $('#imagem').html(response.imagem);
          $('#data').html(response.data);
        },
        error: function(erro) {
          console.log(erro);
        }


      });

    }

    function editarNoticia(perfil) {

      $.ajax({
        type: 'get',
        url: 'noticia/perfil/' + perfil,
        success: function(response) {
          $('#idEdit').val(response.id);
          $('#nomeEdit').val(response.nome);
          $('#descricaoEdit').val(response.descricao);
          $('#dataEdit').val(response.data);


        },
        error: function(erro) {
          console.log(erro);
        }


      });
    }

    function deletarNoticia(perfil) {

      $.ajax({
        type: 'get',
        url: 'noticia/perfil/' + perfil,
        success: function(response) {
          $('#id_noticia').val(response.id);
        },
        error: function(erro) {
          console.log(erro);
        }


      });
    }
  </script>
  @stop