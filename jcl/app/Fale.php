<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fale extends Model
{
    protected $fillable = [
      'email',  'nome', 'assunto', 'mensagem',
    ];

    public function fale()
    {
        return $this->hasOne(fale::class);
    }
}
