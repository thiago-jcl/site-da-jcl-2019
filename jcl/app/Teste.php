<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teste extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'imagem',
    ];

    public function teste()
    {
        return $this->hasOne(teste::class);
    }
}
