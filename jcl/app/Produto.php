<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        'nome','url' ,'descricao','tipo', 'imagem',
    ];

    public function produto()
    {
        return $this->hasOne(produto::class);
    }

}
