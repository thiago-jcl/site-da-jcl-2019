<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'data', 'imagem',
    ];

    public function noticia()
    {
        return $this->hasOne(noticia::class);
    }
}
