<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoUser extends Model
{
    protected $fillable = [
        'nome', 'email', 'cidade', 'estado','curso','telefone'
    ];

    public function cursouser()
    {
        return $this->hasOne(cursouser::class);
    }
}
