<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    protected $fillable = [
        'nome', 'descricao','tipo',
    ];

    public function certificado()
    {
        return $this->hasOne(certificado::class);
    }
}
