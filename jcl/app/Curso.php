<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = [
        'nome', 'carga', 'descricao', 'imagem',
    ];

    public function curso()
    {
        return $this->hasOne(curso::class);
    }


}
