<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parceiros extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'imagem',
    ];

    public function parceiros()
    {
        return $this->hasOne(parceiros::class);
    }
}
