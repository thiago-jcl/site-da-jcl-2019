<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'imagem',
    ];

    public function servico()
    {
        return $this->hasOne(servico::class);
    }

}
