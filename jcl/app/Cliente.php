<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'imagem',
    ];

    public function cliente()
    {
        return $this->hasOne(cliente::class);
    }
}
