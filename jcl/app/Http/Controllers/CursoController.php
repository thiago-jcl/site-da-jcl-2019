<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;

class CursoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $curso = Curso::paginate(5);
 
        return view('Admin.Curso.index')->with('curso', $curso);
    }

    public function cadastrar(Request $request){
        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.curso')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Curso::create([
            'nome' => $request->input('nome'),
            'carga' => $request->input('carga'),
            'descricao' => $request->input('descricao'),
            'imagem' => $upload
        ]);

        return redirect()->route('admin.curso');



    }

    public function perfil($id)
    {
        $perfil = Curso::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Curso::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Curso::find($request->id_curso)->delete();

        return redirect()->back();
    }
}
