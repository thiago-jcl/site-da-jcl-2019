<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $produto = Produto::paginate(5);
        
        return view('Admin.Produto.produto')->with('produto',$produto);
    }
    public function cadastrar(Request $request){

        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.produto')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Produto::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'url' => $request->input('url'),
            'tipo' => $request->input('tipo'),
            'imagem' => $upload
        ]);

        return redirect()->route('admin.produto');



    }

    public function perfil($id)
    {
        $perfil = Produto::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Produto::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Produto::find($request->id_produto)->delete();

        return redirect()->back();
    }

}
