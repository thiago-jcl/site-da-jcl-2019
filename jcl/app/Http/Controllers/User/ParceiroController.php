<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Parceiros;

class ParceiroController extends Controller
{
    public function index()
    {
        $parceiro = Parceiros::paginate(5);
 
        return view('User.Parceiros.index')->with('parceiro', $parceiro);
    }
}
