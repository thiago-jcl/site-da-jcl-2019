<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificado;

class PremController extends Controller
{
    public function index()
    {
        $prem = Certificado::all();
 
        return view('User.Premiacoes.index')->with('prem', $prem);
    }
}
