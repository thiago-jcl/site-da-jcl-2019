<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class TrabalheController extends Controller
{
    public function index()
    {

        return view('User.Trabalhe.index');
    }

    public function cadastrar(Request $request)
    {

        $data = array();
        $data['nome'] = Input::get("nome");
        $data['email'] = Input::get("email");
        $data['telefone'] = Input::get("telefone");
        $data['assunto'] = Input::get("assunto");
        $data['curriculo'] = Input::get("curriculo");
        $data['area'] = Input::get("area");        
        $data['link'] = Input::get("link");

        Mail::send('User.Trabalhe.envio', $data , function ($m) {
           
        $data = array();
        $data['nome'] = Input::get("nome");
        $data['email'] = Input::get("email");
        $data['telefone'] = Input::get("telefone");
        $data['assunto'] = Input::get("assunto");
        $data['curriculo'] = Input::get("curriculo");
        $data['area'] = Input::get("area");
        $data['link'] = Input::get("link");


 $m->from(Input::get('email'), Input::get('nome'));
            $m->to('contato@jcl-tecnologia.com.br');
            $m->subject($data['email']);
		    $messagem ="\n <p><strong>Nome:</strong> {$data['nome']} </p>  \n            <p><strong>Telefone:</strong> {$data['telefone']}</p> \n \n <p><strong>Email:</strong> {$data['email']}</p> \n \n <p><strong>Cargo Desejado:</strong> {$data['area']}</p> \n <p><strong>Linkedin</strong>  <a> {$data['link']} </a> </p> \n <p><strong>Dados do Curriculo:</strong> {$data['curriculo']}</p> \n ";
		    $m->setBody($messagem);

        });

        return redirect()->route('trabalhe');

    }
}
