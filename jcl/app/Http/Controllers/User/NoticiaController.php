<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Noticia;

class NoticiaController extends Controller
{
    public function index()
    {
        $noticia = Noticia::orderby('id','desc')->paginate(5);


        return view('User.Noticia.index')->with('noticia',$noticia);
    }
}
