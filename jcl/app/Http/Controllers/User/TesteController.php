<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teste;

class TesteController extends Controller
{
    public function index()
    {
        $teste = Teste::paginate(5);


        return view('User.Teste.index')->with('teste',$teste);
    }
}
