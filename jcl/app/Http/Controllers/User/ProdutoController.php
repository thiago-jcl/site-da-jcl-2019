<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produto;

class ProdutoController extends Controller
{
    public function index()
    {
        $produto = Produto::orderby('id','desc')->paginate(70);


        return view('User.Produto.index')->with('produto',$produto);
    }
}
