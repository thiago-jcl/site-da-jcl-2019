<?php

namespace App\Http\Controllers\User;

use App\Curso;
use App\CursoUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CursoController extends Controller
{
    public function index()
    {
        $curso = Curso::paginate(5);


        return view('User.Curso.index')->with('curso',$curso);
    }

    public function cadastrar(Request $request){

        CursoUser::create([
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'curso' => $request->input('curso'),
            'telefone' => $request->input('telefone'),
            'cidade' => $request->input('cidade'),
            'estado' => $request->input('estado'),
        ]);

        return redirect()->route('curso');



    }
    }


