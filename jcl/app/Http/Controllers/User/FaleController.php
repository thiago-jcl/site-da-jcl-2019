<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fale;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;


class FaleController extends Controller
{
    public function index()
    {

        return view('User.Fale.index');
    }

    public function cadastrar(Request $request)
    {

        $data = array();
        $data['nome'] = Input::get("nome");
        $data['email'] = Input::get("email");
        $data['assunto'] = Input::get("assunto");
        $data['mensagem'] = Input::get("mensagem");

        Mail::send('User.Fale.envio', $data , function ($m) {
            $m->from(Input::get('email'), Input::get('nome'));
            $m->to('contato@jcl-tecnologia.com.br');
            $m->subject(Input::get('email'));
            $m->setBody(Input::get('mensagem'));

        });

        return redirect()->route('fale');

    }

}
