<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Noticia;

class HomeController extends Controller
{
    public function index(){

        $home = Noticia::all()->sortByDesc('id')->take(2);
        
        return view('welcome')->with('home', $home);
    }

    public function perfil($id)
    {
        $perfil = Noticia::find($id);

        return $perfil;
    }
}
