<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cliente;

class ClienteController extends Controller
{
    public function index()
    {
        $cliente = Cliente::orderBy('id','desc')->paginate(5);
 
        return view('User.Cliente.index')->with('cliente', $cliente);
    }
}
