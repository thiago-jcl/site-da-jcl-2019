<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificado;

class CertController extends Controller
{
    public function index()
    {
        $cert = Certificado::all();
 
        return view('User.Certificacoes.index')->with('cert', $cert);
    }
}
