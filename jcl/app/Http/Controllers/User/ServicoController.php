<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servico;

class ServicoController extends Controller
{
    public function index()
    {
        $servico = Servico::paginate(5);


        return view('User.Servico.index')->with('servico',$servico);
    }
}
