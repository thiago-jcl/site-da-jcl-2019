<?php

namespace App\Http\Controllers;

use App\Parceiros;
use Illuminate\Http\Request;

class ParceiroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $parceiro = Parceiros::paginate(5);
 
        return view('Admin.Parceiros.index')->with('parceiro', $parceiro);
    }

    public function cadastrar(Request $request){
        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.parceiros')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Parceiros::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'imagem' => $upload
        ]);

        return redirect()->route('admin.parceiros');



    }

    public function perfil($id)
    {
        $perfil = Parceiros::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Parceiros::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Parceiros::find($request->id_parceria)->delete();

        return redirect()->back();
    }
}
