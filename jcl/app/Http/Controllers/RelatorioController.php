<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CursoUser;
use App\Curso;

class RelatorioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $relatorio = CursoUser::orderby('id','desc')->paginate(3);
        $curso = Curso::all();
 
        return view('Admin.Relatorio.index')->with('relatorio', $relatorio)->with('curso',$curso);
    }

  
    public function delete(Request $request)
    {
        
         CursoUser::find($request->id_CursoUser)->delete();

        return redirect()->back();
    }
}
