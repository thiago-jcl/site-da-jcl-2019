<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cliente = Cliente::orderby('id','desc')->paginate(5);
 
        return view('Admin.Clientes.index')->with('cliente', $cliente);
    }

    public function cadastrar(Request $request){
        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.cliente')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Cliente::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'imagem' =>$upload
        ]);

        return redirect()->route('admin.cliente');



    }

    public function perfil($id)
    {
        $perfil = Cliente::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Cliente::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Cliente::find($request->id_cliente)->delete();

        return redirect()->back();
    }
}
