<?php

namespace App\Http\Controllers;

use App\Servico;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class ServicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $servico = Servico::paginate(5);


        return view('Admin.Servicos.index')->with('servico',$servico);
    }
    public function cadastrar(Request $request){
 
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
           $name = $request->input('nome');
           $extension = $request->imagem->extension();

           $namefile = "{$name}.{$extension}";

          $upload = $request->imagem->store('imagem');

          if(!$upload){
            return redirect()->route('admin.servico')->with('error','Imagem não é valida');
          }

        }


        Servico::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'imagem' => $upload
        ]);


        return redirect()->route('admin.servico');



    }

    public function perfil($id)
    {
        $perfil = Servico::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Servico::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Servico::find($request->id_servico)->delete();

        return redirect()->back();
    }


}
