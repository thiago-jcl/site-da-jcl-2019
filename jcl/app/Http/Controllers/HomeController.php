<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home = Noticia::all()->sortByDesc('id')->take(2);

        return view('Admin/index')->with('home',$home);
    }

    public function perfil($id)
    {
        $perfil = Noticia::find($id);

        return $perfil;
    }
}
