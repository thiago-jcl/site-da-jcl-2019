<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teste;

class TesteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $teste = Teste::paginate(5);


        return view('Admin.Testes.index')->with('teste',$teste);
    }
    public function cadastrar(Request $request){
        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.teste')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Teste::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'imagem' => $upload
        ]);

        return redirect()->route('admin.teste');



    }

    public function perfil($id)
    {
        $perfil =  Teste::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Teste::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Teste::find($request->id_testes)->delete();

        return redirect()->back();
    }
}
