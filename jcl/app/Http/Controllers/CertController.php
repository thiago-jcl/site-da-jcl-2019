<?php

namespace App\Http\Controllers;

use App\Certificado;
use Illuminate\Http\Request;

class CertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cert = Certificado::paginate(5);
 
        return view('Admin.Certificacoes.index')->with('cert', $cert);
    }

    public function cadastrar(Request $request){
      
        Certificado::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'tipo' => $request->input('tipo'),
            
        ]);

        return redirect()->route('admin.cert');



    }

    public function perfil($id)
    {
        $perfil = Certificado::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Certificado::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Certificado::find($request->id_cert)->delete();

        return redirect()->back();
    }
}

