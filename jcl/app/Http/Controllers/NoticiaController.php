<?php

namespace App\Http\Controllers;

use App\Noticia;
use Illuminate\Http\Request;
use Mockery\Matcher\Not;

class NoticiaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $noticia = Noticia::paginate(5);


        return view('Admin.Noticias.index')->with('noticia',$noticia);
    }
    public function cadastrar(Request $request){
        
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $name = $request->input('nome');
            $extension = $request->imagem->extension();
 
            $namefile = "{$name}.{$extension}";
 
           $upload = $request->imagem->store('imagem');
 
           if(!$upload){
            return redirect()->route('admin.noticia')->with('error','Imagem não é valida');
           }
         }
 
 
     
        Noticia::create([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'data' => $request->input('data'),
            'imagem' => $upload
        ]);

        return redirect()->route('admin.noticia');



    }

    public function perfil($id)
    {
        $perfil = Noticia::find($id);

        return $perfil;
    }

    public function editar(Request $request)
    {
    
        Noticia::find($request->input('editar'))->update($request->all());

        return redirect()->back();
    }
    
    public function delete(Request $request)
    {
        
        Noticia::find($request->id_noticia)->delete();

        return redirect()->back();
    }
}
