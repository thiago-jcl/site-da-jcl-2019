# Site da JCL 2019

# Comandos para rodar o projeto no Servidor

1.Instalar o Composer  
  Windows- **https://getcomposer.org/Composer-Setup.exe** 
  Linux- **php composer-setup.php**

2.Instalar o Laravel
   **composer global require laravel/installer**

3.Instalar o Mysql

4.Subir o projeto pelo WinSCP
  **cd /var/www/html/your-project**

5.Criar uma database com o nome que será utilizado
   **mysql -u root -p**
   **CREATE DATABASE tutorial_database;**

6.Entrar no projeto para configurar
 **cd /var/www/html/your-project** 
  Procurar pelo arquivo **.env** e modificar para a database criada
  
DB_CONNECTION=mysql // Banco padrão
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=root
DB_PASSWORD=

7.Comandos para iniciar
 **php artisan migrate**
 **php artisan db:seed**
 **php artisan storage:link**
 **php artisan cache:clear**

#Configurar o Apache2

1.Permissões nas pastas
  **sudo chgrp -R www-data /var/www/html/your-project**
  **sudo chmod -R 775 /var/www/html/your-project/storage**

2.Criar um arquivo de configuração
      **cd /etc/apache2/sites-available**
      **sudo nano laravel.conf**

  Inserir esse script no arquivo criado

<VirtualHost *:80>
    ServerName yourdomain.tld

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/your-project/public

    <Directory /var/www/html/your-project>
        AllowOverride All
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

3.Desabilitar o serviço padrão do apache e iniciar o serviço criado

**sudo a2dissite 000-default.conf**
**sudo a2ensite laravel.conf**
**sudo a2enmod rewrite**
**sudo service apache2 restart**


Pronto agora sua aplicação está pronta para rodar

